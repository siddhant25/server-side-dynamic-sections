var dynamic_section_server_side = {
    designs: [
        {
            //? All categories
            thumbnail: "preview/edmingle/themed-dynamic-00.png",
            category: "1",
            server_side: true,
            dynamic_section_key: 'all_categories',
            googleFonts: [],
            contentCss: "type-sourcesanspro.css",
            contentClass: "type-sourcesanspro",
            name: "All Categories",
            desc: "Display all categories at once",
            html: `<div class="is-section is-section-100 is-box is-content-top">
        <div class="is-overlay"></div>
        <div class="is-boxes">
            <div name="all-categories-section" class="is-box-centered is-content-top edge-y-1">
                <div class="is-container container-fluid-fluid is-content-1400" style="max-width: 1400px">
                    <div class="row">
                        <div class="col-md-12" style="text-align: left">
                            <h1 style="
                    color: var(--eds-dark);
                    padding-top: 30px;
                    text-align: center;
                  " class="eds-section-heading">
                                All Categories<br />
                            </h1>
                            <p class="eds-section-subheading" style="
                    color: var(--eds-dimdark);
                    text-align: center;
                    margin: 20px auto;
                    max-width: 700px;
                  ">
                                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                                diam nonumy eirmod tempor invidunt ut labore et dolore magna
                                aliquyam erat, sed diam voluptua. At vero eos et accusam et
                                justo duo dolores et ea rebum. Stet clita kasd gubergren.<br />
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-20"></div>
                        </div>
                    </div>
                    <div class="eds-department-section" style="display: grid">
                        <% categories.forEach((category, idx)=> { %>
                            <a class="eds-department" href="#" data-noedit="">
                                <img src="./assets/designs/images/edmingle/person-1.jpg" alt="categories image"
                                    class="eds-department-image" />
                                <div class="eds-department-overlay"></div>
                                <div class="eds-department-details">
                                    <h3>
                                        <%= category %>
                                    </h3>
                                    <p>
                                        <%= course_counts[idx] %>
                                    </p>
                                </div>
                            </a>
                            <% }); %>
                    </div>
                </div>
            </div>
        </div>
    </div>`
        },
        {
            //? Course Instructor
            thumbnail: "preview/edmingle/themed-dynamic-01.png",
            category: "1",
            server_side: true,
            dynamic_section_key: 'course_instructors',
            googleFonts: [],
            contentCss: "type-sourcesanspro.css",
            contentClass: "type-sourcesanspro",
            name: "Course: Instructor",
            desc: "Display course teacher's details",
            html: ` 
            <div class="is-section is-section-100 is-box is-content-top">
            <div class="is-overlay" style="background:var(--eds-light-gradient)"></div>
            <div class="is-boxes">
                <div class="is-box-centered is-content-top edge-y-1">
                    <div class="is-container container-fluid-fluid is-content-1200" style="max-width: 1200px;">
                        <div class="row">
                            <div class="col-md-12" style="text-align: left;">
                                <h1 style="color: var(--eds-dark); text-align: center; padding-top:30px;"
                                    class="eds-section-heading">Course Instructor<br></h1>
                                <p class="eds-section-subheading"
                                    style="color: var(--eds-dimdark); text-align: center; margin: 20px auto; max-width: 700px;">
                                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                                    invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et
                                    accusam
                                    et justo duo dolores et ea rebum. Stet clita kasd gubergren.<br></p>
    
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="spacer height-20"></div>
                            </div>
                        </div>
                        <section class="eds-teacher-section">
                            <% tutors.forEach((tutor)=> { %>
                                <div class="eds-teacher" data-noedit="">
                                    <img src="<%= tutor.img_url %>" alt="categories image" class="eds-teacher-image">
                                    <div>
                                        <h3 class="eds-teacher-name"><%= tutor.tutor_name %></h3>
                                        <p class="eds-teacher-designation">(<%= tutor.tutor_course_count %>)</p>
                                        <p class="eds-teacher-description"><%= tutor.about %></p>
                                    </div>
                                </div>
                                <% }); %>
                    </div>
                </div>
            </div>
        </div>`,
        },
        {
            //? Course short info
            thumbnail: "preview/edmingle/themed-dynamic-02.png",
            category: "1",
            server_side: true,
            dynamic_section_key: course_short_info,
            googleFonts: [],
            contentCss: "type-sourcesanspro.css",
            contentClass: "type-sourcesanspro",
            name: "Course : Short Info",
            desc: "Highlight key points of your course in short.",
            html: `<div class="is-section is-box is-section-auto">
            <div class="is-overlay" style="background:var(--eds-light-gradient)"></div>
            <div class="is-boxes">
                <div class="is-box-centered">
                    <div class="is-container container-fluid-fluid is-content-1400 eds-container-y-space"
                        style="max-width: 1400px; height: 100%; display: flex; flex-direction: column; justify-content: flex-start;">
                        <div class="row">
                            <div class="col-md-6"
                                style="display: flex; flex-direction: column; justify-content: center; align-items: flex-start;">
                                <h1 style="color: var(--eds-dark); text-align: center;" class="eds-section-heading">
                                    Included in this Course</h1>
                                <p class="eds-section-subheading" style="color: var(--eds-dimdark); max-width: 500px;">
                                    Lorem ipsum dolor sit
                                    amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore
                                    et
                                    dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo
                                    dolores
                                    et ea rebum. Stet clita kasd gubergren.</p>
                            </div>
                            <div class="col-md-6" data-noedit="">
                                <div class="eds-info-section">
                                    <div class="eds-info-card">
                                        <div class="eds-info-icon">
                                            <svg width="27" height="27" viewBox="0 0 27 27" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                    d="M19.5827 0C24.0192 0 27 3.2103 27 7.9866V19.0134C27 23.7897 24.0192 27 19.5814 27H7.416C2.97949 27 0 23.7897 0 19.0134V7.9866C0 3.2103 2.97949 0 7.416 0L19.5827 0ZM19.5827 2.025H7.416C4.104 2.025 1.96364 4.36455 1.96364 7.9866V19.0134C1.96364 22.6354 4.104 24.975 7.416 24.975H19.5814C22.8947 24.975 25.0364 22.6354 25.0364 19.0134V7.9866C25.0364 4.36455 22.896 2.025 19.5827 2.025Z"
                                                    fill="white"></path>
                                                <path
                                                    d="M15.2702 12.3968H22.0384C22.1825 12.3968 22.3206 12.3239 22.4225 12.1942C22.5244 12.0646 22.5816 11.8887 22.5816 11.7053C22.5816 11.5219 22.5244 11.346 22.4225 11.2164C22.3206 11.0867 22.1825 11.0138 22.0384 11.0138H15.2702C15.1262 11.0138 14.988 11.0867 14.8861 11.2164C14.7843 11.346 14.7271 11.5219 14.7271 11.7053C14.7271 11.8887 14.7843 12.0646 14.8861 12.1942C14.988 12.3239 15.1262 12.3968 15.2702 12.3968ZM15.2702 14.2983H22.0384C22.1825 14.2983 22.3206 14.2254 22.4225 14.0958C22.5244 13.9661 22.5816 13.7902 22.5816 13.6068C22.5816 13.4234 22.5244 13.2476 22.4225 13.1179C22.3206 12.9882 22.1825 12.9154 22.0384 12.9154H15.2702C15.1262 12.9154 14.988 12.9882 14.8861 13.1179C14.7843 13.2476 14.7271 13.4234 14.7271 13.6068C14.7271 13.7902 14.7843 13.9661 14.8861 14.0958C14.988 14.2254 15.1262 14.2983 15.2702 14.2983ZM15.2702 16.1998H22.0384C22.1825 16.1998 22.3206 16.127 22.4225 15.9973C22.5244 15.8676 22.5816 15.6917 22.5816 15.5083C22.5816 15.325 22.5244 15.1491 22.4225 15.0194C22.3206 14.8897 22.1825 14.8169 22.0384 14.8169H15.2702C15.1262 14.8169 14.988 14.8897 14.8861 15.0194C14.7843 15.1491 14.7271 15.325 14.7271 15.5083C14.7271 15.6917 14.7843 15.8676 14.8861 15.9973C14.988 16.127 15.1262 16.1998 15.2702 16.1998ZM15.2702 10.4952H17.5776C17.7217 10.4952 17.8598 10.4224 17.9617 10.2927C18.0636 10.163 18.1208 9.98716 18.1208 9.80377C18.1208 9.62038 18.0636 9.4445 17.9617 9.31483C17.8598 9.18515 17.7217 9.1123 17.5776 9.1123H15.2702C15.1262 9.1123 14.988 9.18515 14.8861 9.31483C14.7843 9.4445 14.7271 9.62038 14.7271 9.80377C14.7271 9.98716 14.7843 10.163 14.8861 10.2927C14.988 10.4224 15.1262 10.4952 15.2702 10.4952Z"
                                                    fill="white"></path>
                                                <path
                                                    d="M22.0384 16.1998H15.2702C15.1262 16.1998 14.988 16.127 14.8861 15.9973C14.7843 15.8676 14.7271 15.6917 14.7271 15.5083C14.7271 15.325 14.7843 15.1491 14.8861 15.0194C14.988 14.8897 15.1262 14.8169 15.2702 14.8169H22.0384C22.1825 14.8169 22.3206 14.8897 22.4225 15.0194C22.5244 15.1491 22.5816 15.325 22.5816 15.5083C22.5816 15.6917 22.5244 15.8676 22.4225 15.9973C22.3206 16.127 22.1825 16.1998 22.0384 16.1998Z"
                                                    fill="white"></path>
                                                <path
                                                    d="M22.0384 18.089H15.2702C15.1262 18.089 14.988 18.0161 14.8861 17.8865C14.7843 17.7568 14.7271 17.5809 14.7271 17.3975C14.7271 17.2141 14.7843 17.0383 14.8861 16.9086C14.988 16.7789 15.1262 16.7061 15.2702 16.7061H22.0384C22.1825 16.7061 22.3206 16.7789 22.4225 16.9086C22.5244 17.0383 22.5816 17.2141 22.5816 17.3975C22.5816 17.5809 22.5244 17.7568 22.4225 17.8865C22.3206 18.0161 22.1825 18.089 22.0384 18.089Z"
                                                    fill="white"></path>
                                                <path
                                                    d="M4.90918 17.496V9.84118C4.90918 9.27949 5.50616 8.92952 5.98152 9.21254L12.41 13.0399C12.8816 13.3208 12.8816 14.0164 12.41 14.2972L5.98152 18.1246C5.50616 18.4076 4.90918 18.0577 4.90918 17.496Z"
                                                    fill="white"></path>
                                            </svg>
    
                                        </div>
                                        <div class="eds-info-details">
                                            <% let programType; %>
                                                <% if (bundle.is_online_package){ %>
                                                    <% programType='Online' %>
                                                        <% } else { %>
                                                            <% programType='Classroom' %>
                                                                <% } %>
                                                                    <h4>
                                                                        <%= programType %> Program
                                                                    </h4>
                                                                    <p>Branch: <%= bundle.organization_name %> </p>
                                        </div>
    
                                    </div>
                                    <div class="eds-info-card">
                                        <div class="eds-info-icon">
                                            <svg width="28" height="27" viewBox="0 0 28 27" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M1.05 15.61V14.215H2.07C2.67 14.215 3.145 14.105 3.495 13.885C3.845 13.655 4.06 13.3 4.14 12.82H1.05V11.965L2.13 11.89H4.11C4 11.51 3.775 11.24 3.435 11.08C3.095 10.92 2.64 10.84 2.07 10.84H1.05V9.46H6.87V10.375H4.815C5.075 10.545 5.295 10.76 5.475 11.02C5.655 11.28 5.775 11.57 5.835 11.89H6.87V12.82H5.865C5.795 13.51 5.58 14.065 5.22 14.485C4.86 14.895 4.4 15.19 3.84 15.37L6.525 19H4.56L2.175 15.61H1.05Z"
                                                    fill="white"></path>
                                                <path
                                                    d="M19.5581 0C24.7704 0 27.9996 3.23055 27.9996 8.4456V18.5706C27.9996 23.7708 24.7704 27 19.5581 27H9.42903C4.22883 27 1.00098 24.9304 1.00098 20.5C1.00098 19.9303 1.46268 19.4672 2.03373 19.4672C2.60478 19.4672 3.06648 19.9303 3.06648 20.5C3.06648 23 5.32233 24.9304 9.44253 24.9304H19.5581C23.6661 24.9304 25.9341 22.6746 25.9341 18.5665V8.44155C25.9341 4.3335 23.6796 2.0655 19.5581 2.0655H9.44118C5.33313 2.0655 3.5 3.5 3.06551 6.44155V7C3.06551 7.5643 2.60921 8.0206 2.04491 8.0206H2.03006C1.46036 8.0206 0.998656 7.55755 1 6.98785L1 6.44155C1 2.0655 4.25313 0 9.44253 0H19.5581ZM14.4999 6.59313C15.0588 6.59313 15.5124 7.04673 15.5124 7.60563V12.9179L19.5975 15.3533C20.0767 15.6408 20.2347 16.2618 19.9485 16.7424C19.7595 17.0597 19.422 17.2365 19.0778 17.2365C18.9009 17.2365 18.7227 17.1906 18.5594 17.0934L13.9815 14.3624C13.6751 14.1788 13.4874 13.848 13.4874 13.4916V7.60563C13.4874 7.04673 13.941 6.59313 14.4999 6.59313Z"
                                                    fill="white"></path>
                                            </svg>
    
                                        </div>
                                        <div class="eds-info-details">
                                            <h4>Installment Payment</h4>
                                            <p>
                                                <%= bundle.fees_template_ids.length %> intallment plans available
                                            </p>
                                        </div>
                                    </div>
                                    <% if (bundle.free_preview_allowed) { %>
                                        <div class="eds-info-card">
                                            <div class="eds-info-icon">
                                                <svg width="27" height="27" viewBox="0 0 27 27" fill="none"
                                                    xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M7 13.3775C7 13.3775 9.06 9 13.4375 9C17.815 9 19.875 13.3775 19.875 13.3775C19.875 13.3775 17.815 17.755 13.4375 17.755C9.06 17.755 7 13.3775 7 13.3775Z"
                                                        stroke="white" stroke-width="1.5" stroke-linecap="round"
                                                        stroke-linejoin="round"></path>
                                                    <path
                                                        d="M13.4374 14.6648C14.1485 14.6648 14.7249 14.0884 14.7249 13.3773C14.7249 12.6663 14.1485 12.0898 13.4374 12.0898C12.7263 12.0898 12.1499 12.6663 12.1499 13.3773C12.1499 14.0884 12.7263 14.6648 13.4374 14.6648Z"
                                                        fill="white" stroke="white" stroke-width="1.5"
                                                        stroke-linecap="round" stroke-linejoin="round"></path>
                                                    <rect x="1" y="1" width="24.67" height="24.67" rx="6" stroke="white"
                                                        stroke-width="2"></rect>
                                                </svg>
                                            </div>
                                            <div class="eds-info-details">
                                                <h4>Free Preview</h4>
                                                <p>Available</p>
                                            </div>
                                        </div>
                                        <% } %>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>`,
        },
        {
            //? Course Schedule
            thumbnail: "preview/edmingle/themed-dynamic-03.png",
            category: "1",
            googleFonts: [],
            contentCss: "type-sourcesanspro.css",
            contentClass: "type-sourcesanspro",
            name: "Course : Schedule",
            desc: "Display date, time and days of classes",
            html:`
            <div class="is-section is-box is-section-auto">
            <div class="is-overlay"></div>
            <div class="is-boxes">
                <div class="is-box-centered">
                    <div class="is-container container-fluid-fluid is-content-1400 eds-container-y-space"
                        style="max-width: 1400px; height: 100%; display: flex; flex-direction: column; justify-content: flex-start;">
                        <div class="row">
                            <div style="display: flex; flex-direction: column; justify-content: center; align-items: flex-start;"
                                class="col-md-6 order-md-2">
                                <h1 style="color: var(--eds-dark); text-align: center;" class="eds-section-heading">
                                    Schedule of Classes</h1>
                                <p class="eds-section-subheading" style="color: var(--eds-dimdark); max-width: 500px;">
                                    Lorem ipsum dolor sit
                                    amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore
                                    et
                                    dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo
                                    dolores
                                    et ea rebum. Stet clita kasd gubergren.</p>
                                <a href="#" class="eds-icon-link" style="margin-top:20px ;">
                                    <span>
                                        View Detailed Schedule
                                    </span>
                                    <svg width="10" height="10" viewBox="0 0 10 10" fill="none"
                                        style="stroke: var(--eds-primary);" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M9 1L1 9" stroke-width="1.5" stroke-linecap="round"
                                            stroke-linejoin="round"></path>
                                        <path d="M2.33301 1H8.99967V7.66667" stroke-width="1.5" stroke-linecap="round"
                                            stroke-linejoin="round"></path>
                                    </svg>
                                </a>
                            </div>
                            <div class="col-md-6 order-md-1" data-noedit="">
                                
                                <div class=" eds-info-section">
                                    <% if(classData.start_date && classData.end_date){ %>
                                    <div class="eds-info-card">
                                        <div class="eds-info-icon">
                                            <svg width="27" height="29" viewBox="0 0 27 29" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                    d="M19.0583 0.333008C19.6103 0.333008 20.0583 0.781008 20.0583 1.33301L20.0589 2.46342C22.0057 2.5969 23.6226 3.26374 24.767 4.41047C26.0163 5.66514 26.6737 7.46914 26.667 9.63314V21.7971C26.667 26.2398 23.8457 28.9998 19.3057 28.9998H8.02833C3.48833 28.9998 0.666992 26.2011 0.666992 21.6958V9.63047C0.666992 5.44006 3.18307 2.75026 7.28657 2.46388L7.28739 1.33301C7.28739 0.781008 7.73539 0.333008 8.28739 0.333008C8.83939 0.333008 9.28739 0.781008 9.28739 1.33301L9.28699 2.43834H18.0577L18.0583 1.33301C18.0583 0.781008 18.5063 0.333008 19.0583 0.333008ZM24.667 12.205H2.66699V21.6958C2.66699 25.1171 4.57099 26.9998 8.02833 26.9998H19.3057C22.763 26.9998 24.667 25.1518 24.667 21.7971L24.667 12.205ZM19.6019 20.5947C20.1539 20.5947 20.6019 21.0427 20.6019 21.5947C20.6019 22.1467 20.1539 22.5947 19.6019 22.5947C19.0499 22.5947 18.5966 22.1467 18.5966 21.5947C18.5966 21.0427 19.0379 20.5947 19.5899 20.5947H19.6019ZM13.6853 20.5947C14.2373 20.5947 14.6853 21.0427 14.6853 21.5947C14.6853 22.1467 14.2373 22.5947 13.6853 22.5947C13.1333 22.5947 12.6799 22.1467 12.6799 21.5947C12.6799 21.0427 13.1213 20.5947 13.6733 20.5947H13.6853ZM7.75619 20.5947C8.30819 20.5947 8.75619 21.0427 8.75619 21.5947C8.75619 22.1467 8.30819 22.5947 7.75619 22.5947C7.20419 22.5947 6.74953 22.1467 6.74953 21.5947C6.74953 21.0427 7.19219 20.5947 7.74419 20.5947H7.75619ZM19.6019 15.4125C20.1539 15.4125 20.6019 15.8605 20.6019 16.4125C20.6019 16.9645 20.1539 17.4125 19.6019 17.4125C19.0499 17.4125 18.5966 16.9645 18.5966 16.4125C18.5966 15.8605 19.0379 15.4125 19.5899 15.4125H19.6019ZM13.6853 15.4125C14.2373 15.4125 14.6853 15.8605 14.6853 16.4125C14.6853 16.9645 14.2373 17.4125 13.6853 17.4125C13.1333 17.4125 12.6799 16.9645 12.6799 16.4125C12.6799 15.8605 13.1213 15.4125 13.6733 15.4125H13.6853ZM7.75619 15.4125C8.30819 15.4125 8.75619 15.8605 8.75619 16.4125C8.75619 16.9645 8.30819 17.4125 7.75619 17.4125C7.20419 17.4125 6.74953 16.9645 6.74953 16.4125C6.74953 15.8605 7.19219 15.4125 7.74419 15.4125H7.75619ZM18.0577 4.43834H9.28699L9.28739 5.72101C9.28739 6.27301 8.83939 6.72101 8.28739 6.72101C7.73539 6.72101 7.28739 6.27301 7.28739 5.72101L7.28668 4.46861C4.29971 4.71953 2.66699 6.53015 2.66699 9.63047V10.205H24.667L24.667 9.63047C24.6723 7.98381 24.2297 6.70381 23.351 5.82381C22.5797 5.05022 21.4521 4.58821 20.0595 4.46925L20.0583 5.72101C20.0583 6.27301 19.6103 6.72101 19.0583 6.72101C18.5063 6.72101 18.0583 6.27301 18.0583 5.72101L18.0577 4.43834Z"
                                                    fill="white"></path>
                                            </svg>
                                        </div>
                                        <div class="eds-info-details">
                                            <p>Start Date &amp; End Date</p>
                                            <h4><%= classData.start_date %> - <%= classData.end_date %></h4>
                                        </div>
                                    </div>
                                    <% } %>
                                    <div class="eds-info-card">
                                        <div class="eds-info-icon">
                                            <svg width="27" height="28" viewBox="0 0 27 28" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                    d="M19.2204 0.980469C23.7391 0.980469 26.7751 4.15113 26.7751 8.86847V19.7591C26.7751 24.4765 23.7391 27.6471 19.2191 27.6471H7.66173C3.14306 27.6471 0.108398 24.4765 0.108398 19.7591V8.86847C0.108398 4.15113 3.14306 0.980469 7.66173 0.980469H19.2204ZM19.2204 2.98047H7.66173C4.2884 2.98047 2.1084 5.29114 2.1084 8.86847V19.7591C2.1084 23.3365 4.2884 25.6471 7.66173 25.6471H19.2191C22.5937 25.6471 24.7751 23.3365 24.7751 19.7591V8.86847C24.7751 5.29114 22.5951 2.98047 19.2204 2.98047ZM13.4405 7.49194C13.9925 7.49194 14.4405 7.93994 14.4405 8.49194V13.7399L18.4752 16.1439C18.9485 16.4279 19.1045 17.0413 18.8219 17.5159C18.6339 17.8293 18.3019 18.0039 17.9619 18.0039C17.7872 18.0039 17.6112 17.9586 17.4499 17.8639L12.9285 15.1666C12.6272 14.9853 12.4405 14.6586 12.4405 14.3079V8.49194C12.4405 7.93994 12.8885 7.49194 13.4405 7.49194Z"
                                                    fill="white"></path>
                                            </svg>
                                        </div>
                                        <div class="eds-info-details">
                                            <p>Total Time</p>
                                            <h4>24 hrs 32 mins</h4>
                                        </div>
                                    </div>
                                    <div class="eds-info-card">
                                        <div class="eds-info-icon">
                                            <svg width="27" height="27" viewBox="0 0 27 27" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                    d="M3.11898 0C1.41126 0 0.00406001 1.21965 0.00406001 2.69965V5.3993C-0.0158087 6.61622 2.10054 6.61622 2.08067 5.3993V2.69965C2.08067 2.1856 2.52582 1.79977 3.11898 1.79977H6.23186V8.1007C6.23473 8.81993 7.16175 9.24581 7.85016 8.84416L10.3871 7.38361L12.9241 8.84416C13.6121 9.24358 14.5364 8.81844 14.5403 8.1007V2H19.7319C20.7702 2 24.5 3 24.5 7V20.6973C24.5 21.2114 23.442 21.5972 22.8488 21.5972L3.121 21.599C2.75561 21.5989 2.40804 21.6638 2.08067 21.7659V13.4982C2.08868 12.9909 1.61149 12.5781 1.02614 12.5861C0.453501 12.5929 -0.00378045 13.0019 0.00406001 13.4982L1.64183e-07 24.2968C-0.000556264 25.7769 1.41329 27 3.121 27H21.8064C23.5142 27 24.9274 25.7769 24.9274 24.2968V23.3987C25.643 22.7974 26.5 23 26.5 19V6C26.5 3.30035 23.5 0 19.7319 0H3.11898ZM8.30848 1.79977H12.4637V6.42045L10.961 5.55045C10.6127 5.34975 10.1595 5.34975 9.81118 5.55045L8.30848 6.42045V1.79977ZM1.04237 8.99883C0.46893 8.99883 0.00406717 9.4017 0.00406001 9.89871C0.00404557 10.3957 0.468915 10.7986 1.04237 10.7986C1.61582 10.7986 2.08069 10.3957 2.08067 9.89871C2.08066 9.4017 1.6158 8.99883 1.04237 8.99883ZM7.27423 12.5984C5.85839 12.5708 5.85839 14.4257 7.27423 14.3981H15.5746C16.9904 14.4257 16.9904 12.5708 15.5746 12.5984H7.27423ZM19.7319 12.5984C19.1584 12.5984 18.6936 13.0013 18.6936 13.4982C18.6936 13.9952 19.1584 14.3981 19.7319 14.3981C20.3053 14.3981 20.7702 13.9952 20.7702 13.4982C20.7702 13.0013 20.3053 12.5984 19.7319 12.5984ZM7.16674 16.1979C5.78099 16.26 5.88847 18.0615 7.27423 17.9994H19.7298C21.1156 17.9994 21.1156 16.1979 19.7298 16.1979H7.27423C7.23842 16.1963 7.20255 16.1963 7.16674 16.1979ZM3.121 23.3987L22.8488 23.3987V24.2968C22.8488 24.8109 22.3996 25.2002 21.8064 25.2002H3.121C2.52783 25.2002 2.07589 24.8109 2.07966 24.2924C2.08339 23.7787 2.53144 23.3987 3.121 23.3987Z"
                                                    fill="white"></path>
                                            </svg>
    
                                        </div>
                                        <div class="eds-info-details">
                                            <p>Total Classes</p>
                                            <h4><%= classData.total_classes %> Classes </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>`,
        }
        // {
        //     //? Demo content
        //     thumbnail: "preview/edmingle/themed-dynamic-08.png",
        //     category: "1",
        //     googleFonts: [],
        //     contentCss: "type-sourcesanspro.css",
        //     contentClass: "type-sourcesanspro",
        //     name: "Course : Demo Content",
        //     desc: "Advertise some of your content for free",
        //     html: ``,
        // }
    ]
};