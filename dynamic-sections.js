import { SECTIONTYPE } from "./sectionsConstants";

export const dynamicSections = [
  {
    //? All categories - DONE
    thumbnail: "preview/edmingle/themed-dynamic-00.png",
    category: "1",
    googleFonts: [],
    contentCss: "type-sourcesanspro.css",
    contentClass: "type-sourcesanspro",
    sectionType: SECTIONTYPE.THEMEDSECTIONS.DYNAMICSECTIONS,
    serverSide: true, 
    name: "All Categories",
    desc: "Display all categories at once",
    html: `<div class="is-section is-box is-section-100">
    <div class="is-overlay"></div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container container-fluid is-content-1400 space-y-40" style="max-width: 1400px;">
                <div class="row">
                    <div class="col-md-12 center">
                        <h1 class="section-heading heading-gray">All Categories</h1>
                        <p class="ui-main body-gray" style="max-width: 700px; margin:auto">
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
                            tempor
                            invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et
                            accusam
                            et justo duo dolores et ea rebum. Stet clita kasd gubergren.
                        </p>
                    </div>
                </div>
                <%- sectionObj.categoryListing %>
            </div>
        </div>
    </div>
</div>`,
  },
  {
    //? Course Instructor - DONE
    thumbnail: "preview/edmingle/themed-dynamic-01.png",
    category: "1",
    googleFonts: [],
    contentCss: "type-sourcesanspro.css",
    contentClass: "type-sourcesanspro",
    sectionType: SECTIONTYPE.THEMEDSECTIONS.DYNAMICSECTIONS,
    serverSide: true,
    name: "Course: Instructor",
    desc: "Display course teacher's details",
    html: `<div class="is-section is-box is-section-100">
    <div class="is-overlay"></div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container container-fluid is-content-1200 space-y-40" style="max-width: 1200px;">
                <div class="row">
                    <div class="col-md-12 center">
                        <h1 class="section-heading heading-gray">Course Instructor</h1>
                        <p class="ui-main body-gray" style="max-width: 700px; margin:auto">
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
                            tempor
                            invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos
                            et accusam
                            et justo duo dolores et ea rebum. Stet clita kasd gubergren.
                        </p>
                    </div>
                </div>
                <%- element.courseInstuctorListing %>
            </div>
        </div>
    </div>
</div>;`,
  },
  {
    //? Course short info - DONE
    thumbnail: "preview/edmingle/themed-dynamic-02.png",
    category: "1",
    googleFonts: [],
    contentCss: "type-sourcesanspro.css",
    contentClass: "type-sourcesanspro",
    sectionType: SECTIONTYPE.THEMEDSECTIONS.DYNAMICSECTIONS,
    serverSide: true,
    name: "Course : Short Info",
    desc: "Highlight key points of your course in short.",
    html: `<div class="is-section is-box is-section-100">
    <div class="is-overlay bg-light-gradient"></div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container container-fluid is-content-1400 space-t-40" style="max-width: 1400px;">
                <div class="row">
                    <div class="col-md-6"
                        style="display: flex; flex-direction: column; justify-content: center; align-items: flex-start;">
                        <h1 class="section-heading heading-gray">
                            Included in this Course</h1>
                        <p class="ui-main body-gray" style=" max-width: 500px; margin-right:auto ;">
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                            invidunt ut labore
                            et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo
                            dolores
                            et ea rebum. Stet clita kasd gubergren.
                        </p>
                    </div>
                    <div class="col-md-6" data-noedit="">
                        <div class="eds-info-section space-y-40">
                            <div class="eds-info-card">
                                <div class="eds-info-icon">
                                    <svg width="27" height="27" viewBox="0 0 27 27" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M19.5827 0C24.0192 0 27 3.2103 27 7.9866V19.0134C27 23.7897 24.0192 27 19.5814 27H7.416C2.97949 27 0 23.7897 0 19.0134V7.9866C0 3.2103 2.97949 0 7.416 0L19.5827 0ZM19.5827 2.025H7.416C4.104 2.025 1.96364 4.36455 1.96364 7.9866V19.0134C1.96364 22.6354 4.104 24.975 7.416 24.975H19.5814C22.8947 24.975 25.0364 22.6354 25.0364 19.0134V7.9866C25.0364 4.36455 22.896 2.025 19.5827 2.025Z"
                                            fill="white"></path>
                                        <path
                                            d="M15.2702 12.3968H22.0384C22.1825 12.3968 22.3206 12.3239 22.4225 12.1942C22.5244 12.0646 22.5816 11.8887 22.5816 11.7053C22.5816 11.5219 22.5244 11.346 22.4225 11.2164C22.3206 11.0867 22.1825 11.0138 22.0384 11.0138H15.2702C15.1262 11.0138 14.988 11.0867 14.8861 11.2164C14.7843 11.346 14.7271 11.5219 14.7271 11.7053C14.7271 11.8887 14.7843 12.0646 14.8861 12.1942C14.988 12.3239 15.1262 12.3968 15.2702 12.3968ZM15.2702 14.2983H22.0384C22.1825 14.2983 22.3206 14.2254 22.4225 14.0958C22.5244 13.9661 22.5816 13.7902 22.5816 13.6068C22.5816 13.4234 22.5244 13.2476 22.4225 13.1179C22.3206 12.9882 22.1825 12.9154 22.0384 12.9154H15.2702C15.1262 12.9154 14.988 12.9882 14.8861 13.1179C14.7843 13.2476 14.7271 13.4234 14.7271 13.6068C14.7271 13.7902 14.7843 13.9661 14.8861 14.0958C14.988 14.2254 15.1262 14.2983 15.2702 14.2983ZM15.2702 16.1998H22.0384C22.1825 16.1998 22.3206 16.127 22.4225 15.9973C22.5244 15.8676 22.5816 15.6917 22.5816 15.5083C22.5816 15.325 22.5244 15.1491 22.4225 15.0194C22.3206 14.8897 22.1825 14.8169 22.0384 14.8169H15.2702C15.1262 14.8169 14.988 14.8897 14.8861 15.0194C14.7843 15.1491 14.7271 15.325 14.7271 15.5083C14.7271 15.6917 14.7843 15.8676 14.8861 15.9973C14.988 16.127 15.1262 16.1998 15.2702 16.1998ZM15.2702 10.4952H17.5776C17.7217 10.4952 17.8598 10.4224 17.9617 10.2927C18.0636 10.163 18.1208 9.98716 18.1208 9.80377C18.1208 9.62038 18.0636 9.4445 17.9617 9.31483C17.8598 9.18515 17.7217 9.1123 17.5776 9.1123H15.2702C15.1262 9.1123 14.988 9.18515 14.8861 9.31483C14.7843 9.4445 14.7271 9.62038 14.7271 9.80377C14.7271 9.98716 14.7843 10.163 14.8861 10.2927C14.988 10.4224 15.1262 10.4952 15.2702 10.4952Z"
                                            fill="white"></path>
                                        <path
                                            d="M22.0384 16.1998H15.2702C15.1262 16.1998 14.988 16.127 14.8861 15.9973C14.7843 15.8676 14.7271 15.6917 14.7271 15.5083C14.7271 15.325 14.7843 15.1491 14.8861 15.0194C14.988 14.8897 15.1262 14.8169 15.2702 14.8169H22.0384C22.1825 14.8169 22.3206 14.8897 22.4225 15.0194C22.5244 15.1491 22.5816 15.325 22.5816 15.5083C22.5816 15.6917 22.5244 15.8676 22.4225 15.9973C22.3206 16.127 22.1825 16.1998 22.0384 16.1998Z"
                                            fill="white"></path>
                                        <path
                                            d="M22.0384 18.089H15.2702C15.1262 18.089 14.988 18.0161 14.8861 17.8865C14.7843 17.7568 14.7271 17.5809 14.7271 17.3975C14.7271 17.2141 14.7843 17.0383 14.8861 16.9086C14.988 16.7789 15.1262 16.7061 15.2702 16.7061H22.0384C22.1825 16.7061 22.3206 16.7789 22.4225 16.9086C22.5244 17.0383 22.5816 17.2141 22.5816 17.3975C22.5816 17.5809 22.5244 17.7568 22.4225 17.8865C22.3206 18.0161 22.1825 18.089 22.0384 18.089Z"
                                            fill="white"></path>
                                        <path
                                            d="M4.90918 17.496V9.84118C4.90918 9.27949 5.50616 8.92952 5.98152 9.21254L12.41 13.0399C12.8816 13.3208 12.8816 14.0164 12.41 14.2972L5.98152 18.1246C5.50616 18.4076 4.90918 18.0577 4.90918 17.496Z"
                                            fill="white"></path>
                                    </svg>

                                </div>
                                <div class="eds-info-details">
                                    <h4 class="ui-big-bold subheading-gray"><%= sectionObj.packageType %></h4>
                                    <p class="ui-small body-gray">Branch: <%= sectionObj.orgName %></p>
                                </div>
                            </div>
                            <%- sectionObj.installmentInfoCard %>

                            <%- sectionObj.freePreviewInfoCard %> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`,
  },
  {
    //? Course Schedule - DONE
    thumbnail: "preview/edmingle/themed-dynamic-03.png",
    category: "1",
    googleFonts: [],
    contentCss: "type-sourcesanspro.css",
    contentClass: "type-sourcesanspro",
    sectionType: SECTIONTYPE.THEMEDSECTIONS.DYNAMICSECTIONS,
    serverSide: true,
    name: "Course : Schedule",
    desc: "Display date, time and days of classes",
    html: `<div class="is-section is-box is-section-100">
    <div class="is-overlay"></div>
    <div class="is-boxes">
    <div class="is-box-centered">
        <div class="is-container container-fluid is-content-1400 " style="max-width: 1400px;">
            <div class="row">
                <div style="display: flex; flex-direction: column; align-items: flex-start;"
                    class="col-md-6 order-md-2 space-t-40">
                    <h1 class="section-heading heading-gray">
                        Schedule of Classes</h1>
                    <p class="ui-main body-gray" style="max-width: 500px;">
                        Lorem ipsum dolor sit
                        amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore
                        et
                        dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo
                        dolores
                        et ea rebum. Stet clita kasd gubergren.</p>
                    <a href="#" class="eds-icon-link" style="margin-top:20px ;">
                        <span>
                            View Detailed Schedule
                        </span>
                        <svg width="10" height="10" viewBox="0 0 10 10" fill="none"
                            style="stroke: rgba(var(--classic-primary),1);" xmlns="http://www.w3.org/2000/svg">
                            <path d="M9 1L1 9" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
                            </path>
                            <path d="M2.33301 1H8.99967V7.66667" stroke-width="1.5" stroke-linecap="round"
                                stroke-linejoin="round"></path>
                        </svg>
                    </a>
                </div>
                <div class="col-md-6 order-md-1" data-noedit="">
                    <div class=" eds-info-section space-y-40">
                        <div class="eds-info-card">
                            <div class="eds-info-icon">
                                <svg width="27" height="29" viewBox="0 0 27 29" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                        d="M19.0583 0.333008C19.6103 0.333008 20.0583 0.781008 20.0583 1.33301L20.0589 2.46342C22.0057 2.5969 23.6226 3.26374 24.767 4.41047C26.0163 5.66514 26.6737 7.46914 26.667 9.63314V21.7971C26.667 26.2398 23.8457 28.9998 19.3057 28.9998H8.02833C3.48833 28.9998 0.666992 26.2011 0.666992 21.6958V9.63047C0.666992 5.44006 3.18307 2.75026 7.28657 2.46388L7.28739 1.33301C7.28739 0.781008 7.73539 0.333008 8.28739 0.333008C8.83939 0.333008 9.28739 0.781008 9.28739 1.33301L9.28699 2.43834H18.0577L18.0583 1.33301C18.0583 0.781008 18.5063 0.333008 19.0583 0.333008ZM24.667 12.205H2.66699V21.6958C2.66699 25.1171 4.57099 26.9998 8.02833 26.9998H19.3057C22.763 26.9998 24.667 25.1518 24.667 21.7971L24.667 12.205ZM19.6019 20.5947C20.1539 20.5947 20.6019 21.0427 20.6019 21.5947C20.6019 22.1467 20.1539 22.5947 19.6019 22.5947C19.0499 22.5947 18.5966 22.1467 18.5966 21.5947C18.5966 21.0427 19.0379 20.5947 19.5899 20.5947H19.6019ZM13.6853 20.5947C14.2373 20.5947 14.6853 21.0427 14.6853 21.5947C14.6853 22.1467 14.2373 22.5947 13.6853 22.5947C13.1333 22.5947 12.6799 22.1467 12.6799 21.5947C12.6799 21.0427 13.1213 20.5947 13.6733 20.5947H13.6853ZM7.75619 20.5947C8.30819 20.5947 8.75619 21.0427 8.75619 21.5947C8.75619 22.1467 8.30819 22.5947 7.75619 22.5947C7.20419 22.5947 6.74953 22.1467 6.74953 21.5947C6.74953 21.0427 7.19219 20.5947 7.74419 20.5947H7.75619ZM19.6019 15.4125C20.1539 15.4125 20.6019 15.8605 20.6019 16.4125C20.6019 16.9645 20.1539 17.4125 19.6019 17.4125C19.0499 17.4125 18.5966 16.9645 18.5966 16.4125C18.5966 15.8605 19.0379 15.4125 19.5899 15.4125H19.6019ZM13.6853 15.4125C14.2373 15.4125 14.6853 15.8605 14.6853 16.4125C14.6853 16.9645 14.2373 17.4125 13.6853 17.4125C13.1333 17.4125 12.6799 16.9645 12.6799 16.4125C12.6799 15.8605 13.1213 15.4125 13.6733 15.4125H13.6853ZM7.75619 15.4125C8.30819 15.4125 8.75619 15.8605 8.75619 16.4125C8.75619 16.9645 8.30819 17.4125 7.75619 17.4125C7.20419 17.4125 6.74953 16.9645 6.74953 16.4125C6.74953 15.8605 7.19219 15.4125 7.74419 15.4125H7.75619ZM18.0577 4.43834H9.28699L9.28739 5.72101C9.28739 6.27301 8.83939 6.72101 8.28739 6.72101C7.73539 6.72101 7.28739 6.27301 7.28739 5.72101L7.28668 4.46861C4.29971 4.71953 2.66699 6.53015 2.66699 9.63047V10.205H24.667L24.667 9.63047C24.6723 7.98381 24.2297 6.70381 23.351 5.82381C22.5797 5.05022 21.4521 4.58821 20.0595 4.46925L20.0583 5.72101C20.0583 6.27301 19.6103 6.72101 19.0583 6.72101C18.5063 6.72101 18.0583 6.27301 18.0583 5.72101L18.0577 4.43834Z"
                                        fill="white"></path>
                                </svg>
                            </div>
                            <div class="eds-info-details">
                                <p class="ui-small body-gray">Start Date &amp; End Date</p>
                                <h4 class="ui-big-bold subheading-gray"> 
                                    <%= sectionObj.startDate %> - <%= sectionObj.endDate %>
                                </h4>
                            </div>
                        </div>
                        <div class="eds-info-card">
                            <div class="eds-info-icon">
                                <svg width="27" height="27" viewBox="0 0 27 27" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                        d="M3.11898 0C1.41126 0 0.00406001 1.21965 0.00406001 2.69965V5.3993C-0.0158087 6.61622 2.10054 6.61622 2.08067 5.3993V2.69965C2.08067 2.1856 2.52582 1.79977 3.11898 1.79977H6.23186V8.1007C6.23473 8.81993 7.16175 9.24581 7.85016 8.84416L10.3871 7.38361L12.9241 8.84416C13.6121 9.24358 14.5364 8.81844 14.5403 8.1007V2H19.7319C20.7702 2 24.5 3 24.5 7V20.6973C24.5 21.2114 23.442 21.5972 22.8488 21.5972L3.121 21.599C2.75561 21.5989 2.40804 21.6638 2.08067 21.7659V13.4982C2.08868 12.9909 1.61149 12.5781 1.02614 12.5861C0.453501 12.5929 -0.00378045 13.0019 0.00406001 13.4982L1.64183e-07 24.2968C-0.000556264 25.7769 1.41329 27 3.121 27H21.8064C23.5142 27 24.9274 25.7769 24.9274 24.2968V23.3987C25.643 22.7974 26.5 23 26.5 19V6C26.5 3.30035 23.5 0 19.7319 0H3.11898ZM8.30848 1.79977H12.4637V6.42045L10.961 5.55045C10.6127 5.34975 10.1595 5.34975 9.81118 5.55045L8.30848 6.42045V1.79977ZM1.04237 8.99883C0.46893 8.99883 0.00406717 9.4017 0.00406001 9.89871C0.00404557 10.3957 0.468915 10.7986 1.04237 10.7986C1.61582 10.7986 2.08069 10.3957 2.08067 9.89871C2.08066 9.4017 1.6158 8.99883 1.04237 8.99883ZM7.27423 12.5984C5.85839 12.5708 5.85839 14.4257 7.27423 14.3981H15.5746C16.9904 14.4257 16.9904 12.5708 15.5746 12.5984H7.27423ZM19.7319 12.5984C19.1584 12.5984 18.6936 13.0013 18.6936 13.4982C18.6936 13.9952 19.1584 14.3981 19.7319 14.3981C20.3053 14.3981 20.7702 13.9952 20.7702 13.4982C20.7702 13.0013 20.3053 12.5984 19.7319 12.5984ZM7.16674 16.1979C5.78099 16.26 5.88847 18.0615 7.27423 17.9994H19.7298C21.1156 17.9994 21.1156 16.1979 19.7298 16.1979H7.27423C7.23842 16.1963 7.20255 16.1963 7.16674 16.1979ZM3.121 23.3987L22.8488 23.3987V24.2968C22.8488 24.8109 22.3996 25.2002 21.8064 25.2002H3.121C2.52783 25.2002 2.07589 24.8109 2.07966 24.2924C2.08339 23.7787 2.53144 23.3987 3.121 23.3987Z"
                                        fill="white"></path>
                                </svg>
                            </div>
                            <div class="eds-info-details">
                                <p class="ui-small body-gray">Total Classes</p>
                                <h4 class="ui-big-bold subheading-gray"><%= sectionObj.numOfClasses %> Classes </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>`,
  },
  {
    //? Pricing Section -- DONE_CLIENT
    thumbnail: "preview/edmingle/themed-dynamic-04.png",
    category: "1",
    googleFonts: [],
    contentCss: "type-sourcesanspro.css",
    contentClass: "type-sourcesanspro",
    sectionType: SECTIONTYPE.THEMEDSECTIONS.DYNAMICSECTIONS,
    serverSide: false, 
    name: "Course Pricing Options",
    desc: "Display pricing plans for your course",
    html: `
    <div class="is-section is-box is-section-100">
        <div class="is-overlay"></div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container container-fluid is-content-1200 space-y-40" style="max-width: 1200px;">
                    <div class="row">
                        <div class="col-md-12 center">
                            <h1 class="section-heading heading-gray">Our Services has Friendly Packages</h1>
                            <p class="ui-main body-gray" style=" margin: auto; max-width: 968px;">
                                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                                invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam
                                et justo duo dolores et ea rebum. Stet clita kasd gubergren.
                            </p>
                        </div>
                    </div>

                    <div class="row space-t-40">
                        <div class="col-12" data-noedit="">
                            <section class="eds-pricing-section">
                                <form id="pricing-options==<%= uid %>" style="display: none;">
                                    <input type="text" name="bundle_id" value="">
                                    <input type="text" name="institution_bundle_id" value="">
                                </form>

                                <div class="loading-container" style="width: 100%; height: 50vh; display: flex;">
                                    <div class="loader"></div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        if (renderEngineClient) renderEngineClient.pricingOptions(<%= uid %>);
    </script>`,
  },
  {
    //? Lead Capture -- DONE_CLIENT
    thumbnail: "preview/edmingle/themed-dynamic-05.png",
    category: "1",
    googleFonts: [],
    contentCss: "type-sourcesanspro.css",
    contentClass: "type-sourcesanspro",
    sectionType: SECTIONTYPE.THEMEDSECTIONS.DYNAMICSECTIONS,
    serverSide: false, 
    name: "Lead Capture",
    desc: "Collect visitor data and convert to leads",
    html: `
    <div class="is-section is-box is-section-100">
    <div class="is-overlay bg-light-gradient"></div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container container-fluid is-content-1400" style="max-width: 1400px;">
                <div class="row">
                    <div style="display: flex; flex-direction: column; justify-content: center; align-items: flex-start;"
                        class="col-md-6 space-t-40">
                        <h1 class="section-heading heading-gray">
                            Write your Headline here</h1>
                        <p class="ui-main body-gray" style="max-width: 500px;">
                            Lorem ipsum dolor sit
                            amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore
                            et
                            dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo
                            dolores
                            et ea rebum. Stet clita kasd gubergren.</p>
                    </div>
                    <div class="col-md-6 space-y-40" data-noedit="">
                        <section class="eds-lead-section">
                            <form action="<%= window.location.origin %>/contactus" class="eds-lead-form" name="leadformdata">
                                <input type="text" name="firstname" required="" placeholder="Enter your first name.">
                                <input type="text" name="lastname" required="" placeholder="Enter your last name.">
                                <input type="email" name="email" required="" placeholder="Enter your email ID.">
                                <input type="tel" name="phone" required="" placeholder="Enter your phone number.">
                                <textarea name="message" required="" placeholder="Write your Message here."
                                    style="height: 130px;"></textarea>
                                <div class="accept">
                                    <input type="checkbox" name="accept">
                                    <p class="ui-tiny body-gray">Lorem ipsum dolor sit amet, consetetur sadipscing
                                        elitr, </p>
                                </div>

                                <button type="submit" class="eds-btn"
                                    style="color: var(--dark-accent); background: var(--button-color);">Submit</button>
                            </form>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`,
  },
  {
    //? Related Course -- DONE_CLIENT
    thumbnail: "preview/edmingle/themed-dynamic-06.png",
    category: "1",
    googleFonts: [],
    contentCss: "type-sourcesanspro.css",
    contentClass: "type-sourcesanspro",
    sectionType: SECTIONTYPE.THEMEDSECTIONS.DYNAMICSECTIONS,
    serverSide: false, 
    name: "Course : Related Courses",
    desc: "Advertise courses similar to current course",
    html: `
    <div class="is-section is-box is-section-100">
    <div class="is-overlay"></div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container container-fluid space-y-40 is-content-1200" style="max-width: 1200px;">
                <div class="row">
                    <div class="col-md-12 center">
                        <h1 class="section-heading heading-gray">Related Courses</h1>
                        <p class="ui-main body-gray" style="max-width: 968px; margin:auto">
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
                            tempor
                            invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et
                            accusam
                            et justo duo dolores et ea rebum. Stet clita kasd gubergren.
                        </p>
                    </div>
                </div>
                <section class="space-t-40">
                    <div class="swiper" data-noedit="">
                        <div class="swiper-wrapper">
                            <form id="related-courses==<%= uid %>" style="display: none;">
                                <input type="text" name="institution_id" value="">
                                <input type="text" name="curr_course_id" value="">
                            </form>
                            <div class="loading-container" style="width: 100%; height: 50vh; display: flex;">
                                <div class="loader"></div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-pagination"></div>
        </div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
                </section>
        </div>
    </div>
</div>
<script> 
    if (renderEngineClient) renderEngineClient.relatedCourses(<%= uid %>); 
</script>`,
  },
  {
    //? TOP Categories -- DONE_CLIENT
    thumbnail: "preview/edmingle/themed-dynamic-07.png",
    category: "1",
    googleFonts: [],
    contentCss: "type-sourcesanspro.css",
    contentClass: "type-sourcesanspro",
    sectionType: SECTIONTYPE.THEMEDSECTIONS.DYNAMICSECTIONS,
    serverSide: false, 
    name: "Category",
    desc: "Display Category and it's courses",
    html: `
    <div class="is-section is-box is-section-100">
        <div class="is-overlay bg-light-gradient"></div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container container-fluid is-content-1400 space-y-40" style="max-width: 1400px;">
                    <div class="row">
                        <div class="col-md-12 center">
                            <h1 class="section-heading heading-gray">Top Category</h1>
                            <p class="ui-main body-gray" style="max-width: 968px; margin:auto">
                                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
                                tempor
                                invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et
                                accusam
                                et justo duo dolores et ea rebum. Stet clita kasd gubergren.
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer height-60"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div style="text-align: right;">
                                <a href="#"
                                class="ui-small"
                                    style="text-decoration: none; color:rgba(var(--classic-primary),1); display:inline-flex; align-items:center; "
                                    title=""><span style="margin-right:10px;"> See all courses</span> <svg width="10"
                                        height="10" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M9 1L1 9" stroke="#A066FF" stroke-width="1.5" stroke-linecap="round"
                                            stroke-linejoin="round"></path>
                                        <path d="M2.33313 1H8.9998V7.66667" stroke="#A066FF" stroke-width="1.5"
                                            stroke-linecap="round" stroke-linejoin="round"></path>
                                    </svg> </a>
                            </div>
                        </div>
                    </div>
                    <section class="space-t-40 eds-top-category" data-noedit="">
                        <form style="display: none;" id="top-category-course==<%= uid %>">
                            <input type="text" name="institution_id" value="">
                            <input type="text" name="category_id" value="">
                        </form>
        
                        <div class="loading-container" style="width: 100%; height: 50vh; display: flex;">
                            <div class="loader"></div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    <script>
        if (renderEngineClient) renderEngineClient.topCategoryCourses(<%= uid %>);
    </script>
    `,
  },
  {
    //? Demo content -- **DEPRICATED**
    thumbnail: "preview/edmingle/themed-dynamic-08.png",
    category: "1",
    googleFonts: [],
    contentCss: "type-sourcesanspro.css",
    contentClass: "type-sourcesanspro",
    sectionType: SECTIONTYPE.THEMEDSECTIONS.DYNAMICSECTIONS,
    name: "Course : Demo Content",
    desc: "Advertise some of your content for free",
    html: `
      <div class="is-section is-box is-section-100">
        <div class="is-overlay"></div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container container-fluid space-y-40 is-content-1200" style="max-width: 1200px;">
                    <div class="row">
                        <div class="col-md-12 center">
                            <h1 class="section-heading heading-gray">Demo Content</h1>
                            <p class="ui-main body-gray" style="max-width: 968px; margin:auto">
                                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
                                tempor invidunt ut labore
                            </p>
                        </div>
                    </div>
                    <section>
                        <div class="swiper" data-noedit="">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="eds-content-card">
                                        <div class="eds-content-overlay">
                                            <img src="./assets/designs/images/edmingle/course-image.png" alt="course-image">
                                            <div class="overlay-20">
                                                <span>
                                                    <svg width="15" height="19" viewBox="0 0 15 19" fill="none"
                                                        xmlns="http://www.w3.org/2000/svg">
                                                        <path
                                                            d="M12.8762 6.68274C14.3891 7.88382 14.3891 10.1809 12.8762 11.382L5.35695 17.3512C3.39087 18.912 0.491675 17.5118 0.491675 15.0016L0.491676 3.06318C0.491676 0.552901 3.39087 -0.84723 5.35695 0.713553L12.8762 6.68274Z"
                                                            fill="url(#paint0_linear_1001_7924)"></path>
                                                        <defs>
                                                            <linearGradient id="paint0_linear_1001_7924" x1="-1.10144"
                                                                y1="-5.0332" x2="2.70561" y2="33.3098"
                                                                gradientUnits="userSpaceOnUse">
                                                                <stop stop-color="#A066FF"></stop>
                                                                <stop offset="1" stop-color="#6C1FEA"></stop>
                                                            </linearGradient>
                                                        </defs>
                                                    </svg>
                                                </span>
                                                watch video
                                            </div>
                                        </div>
                                        <div class="eds-content-detail">
                                            <p class="ui-small">
                                                Video • 00:44 mins
                                            </p>
                                            <h3 class="ui-big-bold subheading-gray">Edmingle’s Math Masters Classic Course
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="eds-content-card">
                                        <div class="eds-content-overlay">
                                            <img src="./assets/designs/images/edmingle/course-image.png" alt="course-image">
                                            <div class="overlay-20">
                                                <span>
                                                    <svg width="15" height="19" viewBox="0 0 15 19" fill="none"
                                                        xmlns="http://www.w3.org/2000/svg">
                                                        <path
                                                            d="M12.8762 6.68274C14.3891 7.88382 14.3891 10.1809 12.8762 11.382L5.35695 17.3512C3.39087 18.912 0.491675 17.5118 0.491675 15.0016L0.491676 3.06318C0.491676 0.552901 3.39087 -0.84723 5.35695 0.713553L12.8762 6.68274Z"
                                                            fill="url(#paint0_linear_1001_7924)"></path>
                                                        <defs>
                                                            <linearGradient id="paint0_linear_1001_7924" x1="-1.10144"
                                                                y1="-5.0332" x2="2.70561" y2="33.3098"
                                                                gradientUnits="userSpaceOnUse">
                                                                <stop stop-color="#A066FF"></stop>
                                                                <stop offset="1" stop-color="#6C1FEA"></stop>
                                                            </linearGradient>
                                                        </defs>
                                                    </svg>
                                                </span>
                                                watch video
                                            </div>
                                        </div>
                                        <div class="eds-content-detail">
                                            <p class="ui-small">
                                                Video • 00:44 mins
                                            </p>
                                            <h3 class="ui-big-bold subheading-gray">Edmingle’s Math Masters Classic Course
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="eds-content-card">
                                        <div class="eds-content-overlay">
                                            <img src="./assets/designs/images/edmingle/course-image.png" alt="course-image">
                                            <div class="overlay-20">
                                                <span>
                                                    <svg width="15" height="19" viewBox="0 0 15 19" fill="none"
                                                        xmlns="http://www.w3.org/2000/svg">
                                                        <path
                                                            d="M12.8762 6.68274C14.3891 7.88382 14.3891 10.1809 12.8762 11.382L5.35695 17.3512C3.39087 18.912 0.491675 17.5118 0.491675 15.0016L0.491676 3.06318C0.491676 0.552901 3.39087 -0.84723 5.35695 0.713553L12.8762 6.68274Z"
                                                            fill="url(#paint0_linear_1001_7924)"></path>
                                                        <defs>
                                                            <linearGradient id="paint0_linear_1001_7924" x1="-1.10144"
                                                                y1="-5.0332" x2="2.70561" y2="33.3098"
                                                                gradientUnits="userSpaceOnUse">
                                                                <stop stop-color="#A066FF"></stop>
                                                                <stop offset="1" stop-color="#6C1FEA"></stop>
                                                            </linearGradient>
                                                        </defs>
                                                    </svg>
                                                </span>
                                                watch video
                                            </div>
                                        </div>
                                        <div class="eds-content-detail">
                                            <p class="ui-small">
                                                Video • 00:44 mins
                                            </p>
                                            <h3 class="ui-big-bold subheading-gray">Edmingle’s Math Masters Classic Course
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                        <div class="swiper-button-prev round-arrow"></div>
                        <div class="swiper-button-next round-arrow"></div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    `,
  },
  {
    //? Course Review -- ***DEPRICATED***
    thumbnail: "preview/edmingle/themed-dynamic-09.png",
    category: "1",
    googleFonts: [],
    contentCss: "type-sourcesanspro.css",
    contentClass: "type-sourcesanspro",
    sectionType: SECTIONTYPE.THEMEDSECTIONS.DYNAMICSECTIONS,
    name: "Course: Reviews",
    desc: "Show off your happy customer reviews",
    html: `
        <div class="is-section is-box is-section-100">
        <div class="is-overlay bg-light-gradient"></div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container container-fluid is-content-1400 space-y-40" style="max-width: 1400px;">
                    <div class="row">
                        <div class="col-md-12 center">
                            <h1 class="section-heading heading-gray">Reviews</h1>
                            <p class="ui-main body-gray" style="max-width: 968px; margin:auto">
                                10 Reviews Edmingle’s Math Masters Classic Course
                            </p>
                        </div>
                    </div>
                    <section class="eds-review-section">
                        <div class="swiper" data-noedit="">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="eds-review-card">
                                        <h3 class="ui-large-bold subheading-gray">Sonal Khosla</h3>
                                        <div class="star">
                                            <svg width="20" height="19" viewBox="0 0 20 19" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M9.99967 0.833496L12.8322 6.57183L19.1663 7.49766L14.583 11.9618L15.6647 18.2685L9.99967 15.2893L4.33467 18.2685L5.41634 11.9618L0.833008 7.49766L7.16717 6.57183L9.99967 0.833496Z"
                                                    fill="url(#paint0_linear_61_3771)" stroke="url(#paint1_linear_61_3771)"
                                                    stroke-linecap="round" stroke-linejoin="round" />
                                                <defs>
                                                    <linearGradient id="paint0_linear_61_3771" x1="0.833008" y1="9.551"
                                                        x2="19.1663" y2="9.551" gradientUnits="userSpaceOnUse">
                                                        <stop stop-color="#A270F5" />
                                                        <stop offset="1" stop-color="#7A32F0" />
                                                    </linearGradient>
                                                    <linearGradient id="paint1_linear_61_3771" x1="0.833008" y1="9.551"
                                                        x2="19.1663" y2="9.551" gradientUnits="userSpaceOnUse">
                                                        <stop stop-color="#A270F5" />
                                                        <stop offset="1" stop-color="#7A32F0" />
                                                    </linearGradient>
                                                </defs>
                                            </svg>
                                            <svg width="20" height="19" viewBox="0 0 20 19" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M9.99967 0.833496L12.8322 6.57183L19.1663 7.49766L14.583 11.9618L15.6647 18.2685L9.99967 15.2893L4.33467 18.2685L5.41634 11.9618L0.833008 7.49766L7.16717 6.57183L9.99967 0.833496Z"
                                                    fill="url(#paint0_linear_61_3771)" stroke="url(#paint1_linear_61_3771)"
                                                    stroke-linecap="round" stroke-linejoin="round" />
                                                <defs>
                                                    <linearGradient id="paint0_linear_61_3771" x1="0.833008" y1="9.551"
                                                        x2="19.1663" y2="9.551" gradientUnits="userSpaceOnUse">
                                                        <stop stop-color="#A270F5" />
                                                        <stop offset="1" stop-color="#7A32F0" />
                                                    </linearGradient>
                                                    <linearGradient id="paint1_linear_61_3771" x1="0.833008" y1="9.551"
                                                        x2="19.1663" y2="9.551" gradientUnits="userSpaceOnUse">
                                                        <stop stop-color="#A270F5" />
                                                        <stop offset="1" stop-color="#7A32F0" />
                                                    </linearGradient>
                                                </defs>
                                            </svg>
                                            <span class="ui-main-bold">4.0</span>
                                        </div>
                                        <p class="ui-small body-gray">Lorem ipsum dolor sit amet, consetetur sadipscing
                                            elitr, sed diam nonumy
                                            eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam
                                            voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
                                            clita kasd gubergren.</p>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="eds-review-card">
                                        <h3 class="ui-large-bold subheading-gray">Sonal Khosla</h3>
                                        <div class="star">
                                            <svg width="20" height="19" viewBox="0 0 20 19" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M9.99967 0.833496L12.8322 6.57183L19.1663 7.49766L14.583 11.9618L15.6647 18.2685L9.99967 15.2893L4.33467 18.2685L5.41634 11.9618L0.833008 7.49766L7.16717 6.57183L9.99967 0.833496Z"
                                                    fill="url(#paint0_linear_61_3771)" stroke="url(#paint1_linear_61_3771)"
                                                    stroke-linecap="round" stroke-linejoin="round" />
                                                <defs>
                                                    <linearGradient id="paint0_linear_61_3771" x1="0.833008" y1="9.551"
                                                        x2="19.1663" y2="9.551" gradientUnits="userSpaceOnUse">
                                                        <stop stop-color="#A270F5" />
                                                        <stop offset="1" stop-color="#7A32F0" />
                                                    </linearGradient>
                                                    <linearGradient id="paint1_linear_61_3771" x1="0.833008" y1="9.551"
                                                        x2="19.1663" y2="9.551" gradientUnits="userSpaceOnUse">
                                                        <stop stop-color="#A270F5" />
                                                        <stop offset="1" stop-color="#7A32F0" />
                                                    </linearGradient>
                                                </defs>
                                            </svg>
                                            <svg width="20" height="19" viewBox="0 0 20 19" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M9.99967 0.833496L12.8322 6.57183L19.1663 7.49766L14.583 11.9618L15.6647 18.2685L9.99967 15.2893L4.33467 18.2685L5.41634 11.9618L0.833008 7.49766L7.16717 6.57183L9.99967 0.833496Z"
                                                    fill="url(#paint0_linear_61_3771)" stroke="url(#paint1_linear_61_3771)"
                                                    stroke-linecap="round" stroke-linejoin="round" />
                                                <defs>
                                                    <linearGradient id="paint0_linear_61_3771" x1="0.833008" y1="9.551"
                                                        x2="19.1663" y2="9.551" gradientUnits="userSpaceOnUse">
                                                        <stop stop-color="#A270F5" />
                                                        <stop offset="1" stop-color="#7A32F0" />
                                                    </linearGradient>
                                                    <linearGradient id="paint1_linear_61_3771" x1="0.833008" y1="9.551"
                                                        x2="19.1663" y2="9.551" gradientUnits="userSpaceOnUse">
                                                        <stop stop-color="#A270F5" />
                                                        <stop offset="1" stop-color="#7A32F0" />
                                                    </linearGradient>
                                                </defs>
                                            </svg>
                                            <span class="ui-main-bold">4.0</span>
                                        </div>
                                        <p class="ui-small body-gray">Lorem ipsum dolor sit amet, consetetur sadipscing
                                            elitr, sed diam nonumy
                                            eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam
                                            voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
                                            clita kasd gubergren.</p>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="eds-review-card">
                                        <h3 class="ui-large-bold subheading-gray">Sonal Khosla</h3>
                                        <div class="star">
                                            <svg width="20" height="19" viewBox="0 0 20 19" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M9.99967 0.833496L12.8322 6.57183L19.1663 7.49766L14.583 11.9618L15.6647 18.2685L9.99967 15.2893L4.33467 18.2685L5.41634 11.9618L0.833008 7.49766L7.16717 6.57183L9.99967 0.833496Z"
                                                    fill="url(#paint0_linear_61_3771)" stroke="url(#paint1_linear_61_3771)"
                                                    stroke-linecap="round" stroke-linejoin="round" />
                                                <defs>
                                                    <linearGradient id="paint0_linear_61_3771" x1="0.833008" y1="9.551"
                                                        x2="19.1663" y2="9.551" gradientUnits="userSpaceOnUse">
                                                        <stop stop-color="#A270F5" />
                                                        <stop offset="1" stop-color="#7A32F0" />
                                                    </linearGradient>
                                                    <linearGradient id="paint1_linear_61_3771" x1="0.833008" y1="9.551"
                                                        x2="19.1663" y2="9.551" gradientUnits="userSpaceOnUse">
                                                        <stop stop-color="#A270F5" />
                                                        <stop offset="1" stop-color="#7A32F0" />
                                                    </linearGradient>
                                                </defs>
                                            </svg>
                                            <svg width="20" height="19" viewBox="0 0 20 19" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M9.99967 0.833496L12.8322 6.57183L19.1663 7.49766L14.583 11.9618L15.6647 18.2685L9.99967 15.2893L4.33467 18.2685L5.41634 11.9618L0.833008 7.49766L7.16717 6.57183L9.99967 0.833496Z"
                                                    fill="url(#paint0_linear_61_3771)" stroke="url(#paint1_linear_61_3771)"
                                                    stroke-linecap="round" stroke-linejoin="round" />
                                                <defs>
                                                    <linearGradient id="paint0_linear_61_3771" x1="0.833008" y1="9.551"
                                                        x2="19.1663" y2="9.551" gradientUnits="userSpaceOnUse">
                                                        <stop stop-color="#A270F5" />
                                                        <stop offset="1" stop-color="#7A32F0" />
                                                    </linearGradient>
                                                    <linearGradient id="paint1_linear_61_3771" x1="0.833008" y1="9.551"
                                                        x2="19.1663" y2="9.551" gradientUnits="userSpaceOnUse">
                                                        <stop stop-color="#A270F5" />
                                                        <stop offset="1" stop-color="#7A32F0" />
                                                    </linearGradient>
                                                </defs>
                                            </svg>
                                            <span class="ui-main-bold">4.0</span>
                                        </div>
                                        <p class="ui-small body-gray">Lorem ipsum dolor sit amet, consetetur sadipscing
                                            elitr, sed diam nonumy
                                            eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam
                                            voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
                                            clita kasd gubergren.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-pagination"></div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </section>
                </div>
            </div>
        </div>
    </div>
`,
  },
  {
    //? All Coupons -- DONE_CLIENT
    thumbnail: "preview/edmingle/themed-dynamic-10.png",
    category: "1",
    googleFonts: [],
    contentCss: "type-sourcesanspro.css",
    contentClass: "type-sourcesanspro",
    sectionType: SECTIONTYPE.THEMEDSECTIONS.DYNAMICSECTIONS,
    serverSide: false,
    name: "All Coupons",
    desc: "Display coupon code for discounts",
    html: `
    <div class="is-section is-box is-section-100">
        <div class="is-overlay"></div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container container-fluid is-content-1400 space-y-40" style="max-width: 1400px;">
                    <div class="row">
                        <div class="col-md-12 center">
                            <h1 class="section-heading heading-gray">All Coupons
                            </h1>
                            <p class="ui-main body-gray" style="max-width: 768px; margin:auto">
                                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
                                tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero
                                eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren.
                            </p>
                        </div>
                    </div>
                    <section class="eds-coupon-section">
                        <div class="swiper" data-noedit="">
                            <div class="swiper-wrapper">
                                <form style="display: none;" id="all-coupons==<%= uid %>">
                                    <input type="text" name="institution_id" value="">
                                    <input type="text" name="institution_bundle_id" value="">
                                </form>
                                <div class="loading-container" style="width: 100%; height: 50vh; display: flex;">
                                    <div class="loader"></div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    <script>
        if (renderEngineClient) renderEngineClient.allCoupons(<%= uid %>);
    </script>`,
  },
  {
    //? Course Curriculum - DONE 
    thumbnail: "preview/edmingle/themed-dynamic-11.png",
    category: "1",
    googleFonts: [],
    contentCss: "type-sourcesanspro.css",
    contentClass: "type-sourcesanspro",
    sectionType: SECTIONTYPE.THEMEDSECTIONS.DYNAMICSECTIONS,
    serverSide: true,
    name: "COURSE : CURRICULUM",
    desc: "Display course content",
    html: `<div class="is-section is-box is-section-100">
    <div class="is-overlay"></div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container container-fluid is-content-1200 space-y-40" style="max-width: 1200px;">
                <div class="row">
                    <div class="col-md-12 center">
                        <h1 class="section-heading heading-gray">Course Curriculum</h1>
                        <p data-noedit="" class="ui-main" style="color: rgba(var(--classic-primary),1);"> <%= sectionObj.numOfSubjects %> Subjects </p>
                    </div>
                </div>
                <%- sectionObj.curriculumListing %>
            </div>
        </div>
    </div>
</div>`,
  },
  {
    //? Course on sale -- DONE_CLIENT
    thumbnail: "preview/edmingle/themed-dynamic-12.png",
    category: "1",
    googleFonts: [],
    contentCss: "type-sourcesanspro.css",
    contentClass: "type-sourcesanspro",
    sectionType: SECTIONTYPE.THEMEDSECTIONS.DYNAMICSECTIONS,
    serverSide: false, 
    name: "Need to change name here",
    desc: "Need to change desc here",
    html: `
    <div class="is-section is-box is-section-100">
        <div class="is-overlay bg-light-gradient"></div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container container-fluid is-content-1400 space-y-40" style="max-width: 1400px;">
                    <div class="row">

                        <div class="col-md-6" data-noedit="">
                            <form id="course-on-sale-course==<%= uid %>" style="display: none;">
                                <input type="text" name="institution_bundle_id" value="204">
                            </form>
                            <div class="loading-container" style="width: 100%; height: 50vh; display: flex;">
                                <div class="loader"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <form id="course-on-sale-promo==<%= uid %>" style="display: none;">
                                <input type="text" name="promo_code_id" value="">
                            </form>
                            <div id="loading-container==5" class="loading-container" style="width: 100%; height: 50vh; display: flex;">
                                <div class="loader"></div>
                            </div>

                            <h1 style="max-width: 500px;" class="section-heading heading-gray">Write
                                your Headline
                                here
                            </h1>
                            <p style="max-width: 500px;" class="ui-main body-gray">
                                Lorem ipsum dolor
                                sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                                labore et dolore magna aliquyam erat, sed diam voluptua.
                            </p>
                            <div class="spacer height-20"></div>
                            <div class="eds-btn-group" style="text-align: left;">
                                <a id="course-on-sale==<%= uid %>==course-link" href="#" class="eds-btn primary" style="display: inline-block;">
                                    Enroll now
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        if (renderEngineClient) renderEngineClient.courseOnSale(<%= uid %>);
    </script>`,
  },
  {
    //? Course Banner - DONE
    thumbnail: "preview/edmingle/themed-dynamic-13.png",
    category: "1",
    googleFonts: [],
    contentCss: "type-sourcesanspro.css",
    contentClass: "type-sourcesanspro",
    sectionType: SECTIONTYPE.THEMEDSECTIONS.DYNAMICSECTIONS,
    serverSide: true,
    name: "COURSE: BANNER",
    desc: "Introduce your course to the users",
    html: `<div class="is-section is-box is-section-100">
    <div class="is-overlay bg-secondary-gradient"></div>
    <div class="is-boxes">
        <div class="is-box-centered ">
            <div class="is-container container-fluidis-content-1400 space-y-60" style="max-width: 1400px;">
                <div class="row eds-banner-section">
                    <div class="col-md-6">
                        <div class="course-details" style="max-width: 500px;">
                            <img src="assets/designs/images/edmingle/dots-left.png" alt="dots bg" class="dots-left">
                            <h1 style="z-index:2" class="section-heading heading-white">
                                <%= sectionObj.courseTitle %> 
                            </h1>
                            <p style="z-index:2" class="ui-main body-white">
                                <%= sectionObj.courseDescription %> 
                            </p>

                            <div class="course-review">
                                <%- sectionObj.courseBannerRating %>
                                <%- sectionObj.courseBannerInstructorList %>
                            </div>
                            
                            <%- sectionObj.courseBannerPricing %>
                            
                            <div class="eds-btn-group" style="text-align: left;">
                                <a href="<%= sectionObj.courseBannerPurchaseLink %>" class="eds-btn white" style="display: inline-block;">Buy Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 eds-spaced-hero course-video">
                        <img src="assets/designs/images/edmingle/dots-right.png" alt="dots bg" class="dots-right">
                        <div class="embed-responsive embed-responsive-16by9"
                            style="border:10px solid var(--dark-accent); border-radius:20px;">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/P5yHEKqx86U?rel=0"
                                frameborder="0"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen=""></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
`,
  },
  {
    //? Course Overview - DONE
    thumbnail: "preview/edmingle/themed-dynamic-14.png",
    category: "1",
    googleFonts: [],
    contentCss: "type-sourcesanspro.css",
    contentClass: "type-sourcesanspro",
    sectionType: SECTIONTYPE.THEMEDSECTIONS.DYNAMICSECTIONS,
    serverSide: true,
    name: "COURSE : OVERVIEW",
    desc: "Describe the topics that the course covers",
    html: `<div class="is-section is-box is-section-100">
    <div class="is-overlay bg-light-gradient"></div>
    <div class="is-boxes">
    <div class="is-box-centered">
        <div class="is-container container-fluid is-content-1400 space-y-40" style="max-width: 1400px;">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="heading-gray section-heading">
                        Course Overview
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="spacer height-20"></div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12" data-noedit="">
                    <section class="eds-prose-section">
                        <%- sectionObj.courseOverview %> 
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
</div>`,
  },
  {
    //? Refer and Earn -- DONE_CLIENT
    thumbnail: "preview/edmingle/themed-dynamic-15.png",
    category: "1",
    googleFonts: [],
    contentCss: "type-sourcesanspro.css",
    contentClass: "type-sourcesanspro",
    sectionType: SECTIONTYPE.THEMEDSECTIONS.DYNAMICSECTIONS,
    serverSide: false,
    name: "REFER AND EARN",
    desc: "Refer friends and family to get rewards",
    html: `
    <div class="is-section is-box is-section-100">
        <div class="is-overlay bg-secondary-gradient"></div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container container-fluidis-content-1400 space-y-40" style="max-width: 1400px;">
                    <div class="row">
                        <form id="refer-n-earn==<%= uid %>" style="display: none;">
                            <input type="text" name="org_id" value="">
                        </form>

                        <div id="loading-container==<%= uid %>" class="loading-container" style="width: 100%; height: 50vh; display: flex;">
                            <div class="loader"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script defer>
        if (renderEngineClient) renderEngineClient.referAndEarn(<%= uid %>);
    </script>`,
  },
  {
    //?contact us form section -- DONE_CLIENT
    thumbnail: "preview/edmingle/themed-page-01.png",
    category: "1",
    googleFonts: [],
    contentCss: "type-sourcesanspro.css",
    contentClass: "type-sourcesanspro",
    sectionType: SECTIONTYPE.THEMEDSECTIONS.DYNAMICSECTIONS,
    serverSide: true,
    name: "need to change name here",
    desc: "need to change desc here",
    html: ` 
      <div class="is-section is-box is-section-100">
          <div class="is-overlay"></div>
          <div class="is-boxes">
              <div class="is-box-centered">
                  <div class="is-container container-fluid is-content-760" style="max-width: 760px;">
                      <div class="row">
                          <div class="col-md-6 space-t-40 contact-form-heading">
                              <h2 class="heading-gray">
                                  Reach us for any help</h2>
                              <p class="ui-small subheading-gray">
                                  You can reach us anytime via
                                  <a href="mailto:iskconbhagvata@edmingle.com"
                                      class="mail-link">iskconbhagvata@edmingle.com</a>
                              </p>
                          </div>
                          <div class="col-md-6 space-y-40" data-noedit="">
                              <section>
                                  <form action="<%= window.location.origin %>/contactus" class="contact-form" name="contact-us-formdata">
                                      <label class="ui-tiny">
                                          Name
                                          <input type="text" name="name" required="" placeholder="Enter your name.">
                                      </label>
                                      <label class="ui-tiny">
                                          Email
                                          <input type="email" name="email" required="" placeholder="Enter your email ID.">
                                      </label>
                                      <label class="ui-tiny">
                                          Phone
                                          <input type="tel" name="phone" required="" placeholder="Enter your phone number.">
                                      </label>
                                      <label class="ui-tiny">
                                          Desription
                                          <textarea name="message" required="" placeholder="Write your Message here."
                                              style="height: 130px;"></textarea>
                                      </label>
                                      <button type="submit" class="eds-btn"
                                          style="color: var(--dark-accent); background: var(--button-color);">Submit</button>
                                  </form>
                              </section>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      `,
  },
];
