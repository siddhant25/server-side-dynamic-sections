var RELTED_COURSES = `
        <div class="is-section is-box is-section-100">
            <div class="is-overlay"></div>
            <div class="is-boxes">
                <div class="is-box-centered">
                    <div class="is-container container-fluid space-y-40 is-content-1200" style="max-width: 1200px;">
                        <div class="row">
                            <div class="col-md-12 center">
                                <h1 class="section-heading heading-gray">Related Courses</h1>
                                <p class="ui-main body-gray" style="max-width: 968px; margin:auto">
                                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
                                    tempor
                                    invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et
                                    accusam
                                    et justo duo dolores et ea rebum. Stet clita kasd gubergren.
                                </p>
                            </div>
                        </div>
                        <section class="space-t-40">
                            <div class="swiper" data-noedit="">
                                <div class="swiper-wrapper">
                                    <form id="related-courses==<%= uid %>" style="display: none;">
                                        <input type="text" name="institution_id" value="">
                                        <input type="text" name="curr_course_id" value="">
                                    </form>
                                    <div class="loading-container" style="width: 100%; height: 50vh; display: flex;">
                                        <div class="loader"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                    </div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                    </section>
                </div>
            </div>
        </div>
        <script> 
            if (renderEngineClient) renderEngineClient.relatedCourses(<%= uid %>); 
        </script>
        `;

var ALL_COUPONS = `<div class="is-section is-box is-section-100">
<div class="is-overlay"></div>
<div class="is-boxes">
    <div class="is-box-centered">
        <div class="is-container container-fluid is-content-1400 space-y-40" style="max-width: 1400px;">
            <div class="row">
                <div class="col-md-12 center">
                    <h1 class="section-heading heading-gray">All Coupons
                    </h1>
                    <p class="ui-main body-gray" style="max-width: 768px; margin:auto">
                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
                        tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero
                        eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren.
                    </p>
                </div>
            </div>
            <section class="eds-coupon-section">
                <div class="swiper" data-noedit="">
                    <div class="swiper-wrapper">
                        <form style="display: none;" id="all-coupons==<%= uid %>">
                            <input type="text" name="institution_id" value="1">
                            <input type="text" name="institution_bundle_id" value="">
                        </form>
                        <div class="loading-container" style="width: 100%; height: 50vh; display: flex;">
                            <div class="loader"></div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
</div>
<script>
    if (renderEngineClient) renderEngineClient.allCoupons(<%= uid %>);
</script>`;

var CATEGORY_COURSES = `    <div class="is-section is-box is-section-100">
<div class="is-overlay bg-light-gradient"></div>
<div class="is-boxes">
    <div class="is-box-centered">
        <div class="is-container container-fluid is-content-1400 space-y-40" style="max-width: 1400px;">
            <div class="row">
                <div class="col-md-12 center">
                    <h1 class="section-heading heading-gray">Top Category</h1>
                    <p class="ui-main body-gray" style="max-width: 968px; margin:auto">
                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
                        tempor
                        invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et
                        accusam
                        et justo duo dolores et ea rebum. Stet clita kasd gubergren.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="spacer height-60"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div style="text-align: right;">
                        <a href="#"
                        class="ui-small"
                            style="text-decoration: none; color:rgba(var(--classic-primary),1); display:inline-flex; align-items:center; "
                            title=""><span style="margin-right:10px;"> See all courses</span> <svg width="10"
                                height="10" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9 1L1 9" stroke="#A066FF" stroke-width="1.5" stroke-linecap="round"
                                    stroke-linejoin="round"></path>
                                <path d="M2.33313 1H8.9998V7.66667" stroke="#A066FF" stroke-width="1.5"
                                    stroke-linecap="round" stroke-linejoin="round"></path>
                            </svg> </a>
                    </div>
                </div>
            </div>
            <section class="space-t-40 eds-top-category" data-noedit="">
                <form style="display: none;" id="top-category-course==<%= uid %>">
                    <input type="text" name="institution_id" value="1">
                    <input type="text" name="category_id" value="1">
                </form>

                <div class="loading-container" style="width: 100%; height: 50vh; display: flex;">
                    <div class="loader"></div>
                </div>
            </section>
        </div>
    </div>
</div>
</div>
<script>
if (renderEngineClient) renderEngineClient.topCategoryCourses(<%= uid %>);
</script>
`;


var COURSE_ON_SALE = `
<div class="is-section is-box is-section-100">
        <div class="is-overlay bg-light-gradient"></div>
        <div class="is-boxes">
            <div class="is-box-centered">
                <div class="is-container container-fluid is-content-1400 space-y-40" style="max-width: 1400px;">
                    <div class="row">

                        <div class="col-md-6" data-noedit="">
                            <form id="course-on-sale-course==<%= uid %>" style="display: none;">
                                <input type="text" name="institution_bundle_id" value="204">
                            </form>
                            <div class="loading-container" style="width: 100%; height: 50vh; display: flex;">
                                <div class="loader"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <form id="course-on-sale-promo==<%= uid %>" style="display: none;">
                                <input type="text" name="promo_code_id" value="">
                            </form>
                            <div id="loading-container==5" class="loading-container" style="width: 100%; height: 50vh; display: flex;">
                                <div class="loader"></div>
                            </div>

                            <h1 style="max-width: 500px;" class="section-heading heading-gray">Write
                                your Headline
                                here
                            </h1>
                            <p style="max-width: 500px;" class="ui-main body-gray">
                                Lorem ipsum dolor
                                sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                                labore et dolore magna aliquyam erat, sed diam voluptua.
                            </p>
                            <div class="spacer height-20"></div>
                            <div class="eds-btn-group" style="text-align: left;">
                                <a id="course-on-sale==<%= uid %>==course-link" href="#" class="eds-btn primary" style="display: inline-block;">
                                    Enroll now
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        if (renderEngineClient) renderEngineClient.courseOnSale(<%= uid %>);
    </script>

`;

var LEAD_CAPTURE = `
<div class="is-section is-box is-section-100">
    <div class="is-overlay bg-light-gradient"></div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container container-fluid is-content-1400" style="max-width: 1400px;">
                <div class="row">
                    <div style="display: flex; flex-direction: column; justify-content: center; align-items: flex-start;"
                        class="col-md-6 space-t-40">
                        <h1 class="section-heading heading-gray">
                            Write your Headline here</h1>
                        <p class="ui-main body-gray" style="max-width: 500px;">
                            Lorem ipsum dolor sit
                            amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore
                            et
                            dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo
                            dolores
                            et ea rebum. Stet clita kasd gubergren.</p>
                    </div>
                    <div class="col-md-6 space-y-40" data-noedit="">
                        <section class="eds-lead-section">
                            <form action="<%= window.location.origin %>/contactus" class="eds-lead-form" name="leadformdata">
                                <input type="text" name="firstname" required="" placeholder="Enter your first name.">
                                <input type="text" name="lastname" required="" placeholder="Enter your last name.">
                                <input type="email" name="email" required="" placeholder="Enter your email ID.">
                                <input type="tel" name="phone" required="" placeholder="Enter your phone number.">
                                <textarea name="message" required="" placeholder="Write your Message here."
                                    style="height: 130px;"></textarea>
                                <div class="accept">
                                    <input type="checkbox" name="accept">
                                    <p class="ui-tiny body-gray">Lorem ipsum dolor sit amet, consetetur sadipscing
                                        elitr, </p>
                                </div>

                                <button type="submit" class="eds-btn"
                                    style="color: var(--dark-accent); background: var(--button-color);">Submit</button>
                            </form>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`;

var PRICING_OPTIONS = `
<div class="is-section is-box is-section-100">
    <div class="is-overlay"></div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container container-fluid is-content-1200 space-y-40" style="max-width: 1200px;">
                <div class="row">
                    <div class="col-md-12 center">
                        <h1 class="section-heading heading-gray">Our Services has Friendly Packages</h1>
                        <p class="ui-main body-gray" style=" margin: auto; max-width: 968px;">
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                            invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam
                            et justo duo dolores et ea rebum. Stet clita kasd gubergren.
                        </p>
                    </div>
                </div>

                <div class="row space-t-40">
                    <div class="col-12" data-noedit="">
                        <section class="eds-pricing-section">
                            <form id="pricing-options==<%= uid %>" style="display: none;">
                                <input type="text" name="bundle_id" value="188">
                                <input type="text" name="institution_bundle_id" value="204">
                            </form>

                            <div class="loading-container" style="width: 100%; height: 50vh; display: flex;">
                                <div class="loader"></div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    if (renderEngineClient) renderEngineClient.pricingOptions(<%= uid %>);
</script>`;

var REFER_N_EARN = `
<div class="is-section is-box is-section-100">
    <div class="is-overlay bg-secondary-gradient"></div>
    <div class="is-boxes">
        <div class="is-box-centered">
            <div class="is-container container-fluidis-content-1400 space-y-40" style="max-width: 1400px;">
                <div class="row">
                    <form id="refer-n-earn==<%= uid %>" style="display: none;">
                        <input type="text" name="org_id" value="2">
                    </form>

                    <div id="loading-container==<%= uid %>" class="loading-container" style="width: 100%; height: 50vh; display: flex;">
                        <div class="loader"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
<script defer>
    if (renderEngineClient) renderEngineClient.referAndEarn(<%= uid %>);
</script>`;







