endpointMapper = {
    getTutorCourses: (paramObj) => {
        return `institute/${paramObj.institution_id}/courses?tutor_ids=${paramObj.tutor_ids},`;
    },
    getCourseDetails: (paramObj) => {
        return `organization/bundles/${paramObj.institution_bundle_id}?get_tutors=1&get_promocode=1`;
    },
    getAllCoupons: () => {
        return `mobile/metadata`;
    },
    getAllCourseCoupons: (paramObj) => {
        return `organization/bundles/${paramObj.institution_bundle_id}?get_tutors=1&get_promocode=1`;
    },
    getCategoryCourses: (paramObj) => {
        return `institute/${paramObj.institution_id}/courses?categories_ids=${paramObj.category_id}`;
    },
    getCourseOnSaleData: (paramObj) => {
        return `builder/courseonsale?institution_bundle_id=${paramObj.institution_bundle_id}` + `${paramObj.promo_code_id ? '&promo_code_id=' + paramObj.promo_code_id : ''}`;
    },
    getPricingOptions: (paramObj) => {
        return `public/bundles/${paramObj.bundle_id}/organizations?institution_bundle_id=${paramObj.institution_bundle_id}`;
    },
    getCreditVariables: (paramObj = null) => {
        return `edcredits/variables`;
    },
    getUserReferral: (paramObj) => {
        return `users/edcredits`;
    },
    getAllCategories: (paramObj) => {
        return `builder/institute/${paramObj.institution_id}/course/categories`;
    }
}

var RELATED_COURSE_LISTING = `
<% relatedCourses.forEach((course) => { %> 
    <div class="swiper-slide">
        <a class="eds-course-card" href="<%= window.location.origin %>/course/<%= course.pretty_name %>-<%= course.institution_bundle_id %>">
            
            <img src="<%= course.img_url ? course.img_url : "https://edmingle.b-cdn.net/edmingle_websitebuilder/course-default.png" %>" alt="course-image">
            <div>
                <h3 class="eds-course-title ui-big-bold subheading-gray">
                    <%= course.bundle_name %>
                </h3>

                <% if (course.rating > 0) { %> 
                    <div class="eds-course-rating">
                        <span class="ui-main"><%= course.rating %> </span>
                        <% let Rating = course.rating; %>
                        <% while(Rating > 0){ %>
                            <% if (Rating >= 1) { %> 
                                <svg width="18" height="17" viewBox="0 0 18 17" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M9.00021 0.625L6.44084 5.81125L0.720215 6.63812L4.86022 10.6769L3.88146 16.375L9.00021 13.6862L14.119 16.375L13.1402 10.6769L17.2802 6.64375L11.5596 5.81125L9.00021 0.625Z"
                                        fill="url(#paint0_linear_254_607)" />
                                    <defs>
                                        <linearGradient id="paint0_linear_254_607" x1="0.720215" y1="8.5"
                                            x2="17.2802" y2="8.5" gradientUnits="userSpaceOnUse">
                                            <stop stop-color="#A270F5" />
                                            <stop offset="1" stop-color="#7A32F0" />
                                        </linearGradient>
                                    </defs>
                                </svg>
                        <% } else { %> 
                            <svg width="9" height="17" viewBox="0 0 9 17" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M6.44084 5.81125L0.720215 6.64375L4.86022 10.6769L3.88147 16.375L9.00022 13.6862V0.625L6.44084 5.81125Z"
                                    fill="url(#paint0_linear_254_1011)" />
                                <defs>
                                    <linearGradient id="paint0_linear_254_1011" x1="0.720215" y1="8.5"
                                        x2="9.00022" y2="8.5" gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#A270F5" />
                                        <stop offset="1" stop-color="#7A32F0" />
                                    </linearGradient>
                                </defs>
                            </svg>
                        <% } %>
                        <% Rating -= 1; %>        
                    <% } %>
                    <span class="count ui-main">(<%= course.num_of_rating; %>)</span>
                </div>
                <% } %>

                <p class="ui-tiny-italic body-gray eds-course-subtitle">
                    <% function printTutors(tutors) {
                        let tutorString = ""
                        tutors.forEach((tutor, idx) => {
                            tutorString += tutor.tutor_name;
                            if (idx == course.tutors.length - 1){
                                return tutorString;
                            }
                            tutorString += ', ';
                        }); 
                    }%>
                    <%= printTutors(course.tutors) %>
                </p>
                <% function displayPrice(position, cost, symbol){
                    if (cost == 0){
                        return "FREE";
                    } else {
                        if (position == 1){
                            return symbol + cost;
                        }
                        else {
                            return cost + symbol;
                        }
                    }
                } %>
                <p class="eds-course-price">
                    <span>
                       <%= displayPrice(course.position, course.cost, course.currency_symbol) %>
                    </span>
                <% if (Number(course.striked_cost) > 0){ %> 
                    <span class="striked-price">
                        <%= displayPrice(course.position, course.striked_cost, course.currency_symbol) %>
                    </span>
                <% } %> 
                </p>
            </div>
        </a>
    </div>
<% }); %>`;

var COUPONS_LISTING = `<% coupons.forEach(coupon => { %> 
    <div class="swiper-slide">
        <div class="eds-coupon-card">
            <div class="back"></div>
            <div class="front">
                <div class="coupon">
                    <h3><%= coupon.discount %>% OFF</h3>
                    <h4><%= coupon.promo_code_text %></h4>
                </div>
                <p class="ui-micro-bold body-gray expiry">
                    <span>MAX ₹<%= coupon.max_discount_amount %></span>
                    <span>EXPIRES ON <%= coupon.promo_code_end_time %></span>
                </p>
                <p class="ui-small-bold subheading-gray occation">
                    <%= coupon.promo_code_description %>
                </p>
                <div class="collect ui-tiny">
                    <span>Applicable on <%= coupon.institution_bundle_ids.length %>  Courses</span>
                    <% if(course){ %>
                        <a href="<%= window.location.origin %>/course/<%= course.pretty_name %>-<%= course.institution_bundle_id %>/checkout?coupon_id=<%= coupon.promo_code_id %>">Redeem Now</a>
                    <% } %>
                </div>
            </div>
        </div>
    </div>
<% }) %>`;

var CATEGORY_COURSE_LISTING = `
    <a class="eds-course-card" href="<%= window.location.origin %>/course/<%= course.pretty_name %>-<%= course.institution_bundle_id %>" data-noedit="">
        <img src="<%= course.img_url ? course.img_url : "https://edmingle.b-cdn.net/edmingle_websitebuilder/course-default.png" %>" alt="course-image">
        <div>
            <h3 class="eds-course-title ui-big-bold subheading-gray">
                <%= course.bundle_name %>
            </h3>

            <% if (course.rating > 0) { %> 
                <div class="eds-course-rating">
                    <span class="ui-main"><%= course.rating %></span>
                    <% let Rating = course.rating; %>
                    <% while(Rating > 0){ %>
                        <% if (Rating >= 1) { %> 
                            <svg width="18" height="17" viewBox="0 0 18 17" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M9.00021 0.625L6.44084 5.81125L0.720215 6.63812L4.86022 10.6769L3.88146 16.375L9.00021 13.6862L14.119 16.375L13.1402 10.6769L17.2802 6.64375L11.5596 5.81125L9.00021 0.625Z"
                                    fill="url(#paint0_linear_254_607)" />
                                <defs>
                                    <linearGradient id="paint0_linear_254_607" x1="0.720215" y1="8.5"
                                        x2="17.2802" y2="8.5" gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#A270F5" />
                                        <stop offset="1" stop-color="#7A32F0" />
                                    </linearGradient>
                                </defs>
                            </svg>
                    <% } else { %> 
                        <svg width="9" height="17" viewBox="0 0 9 17" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M6.44084 5.81125L0.720215 6.64375L4.86022 10.6769L3.88147 16.375L9.00022 13.6862V0.625L6.44084 5.81125Z"
                                fill="url(#paint0_linear_254_1011)" />
                            <defs>
                                <linearGradient id="paint0_linear_254_1011" x1="0.720215" y1="8.5"
                                    x2="9.00022" y2="8.5" gradientUnits="userSpaceOnUse">
                                    <stop stop-color="#A270F5" />
                                    <stop offset="1" stop-color="#7A32F0" />
                                </linearGradient>
                            </defs>
                        </svg>
                    <% } %>
                    <% Rating -= 1; %>        
                <% } %>
                <span class="count ui-main">(<%= course.num_of_rating; %>)</span>
            </div>
            <% } %>
            
            <p class="ui-tiny-italic body-gray eds-course-subtitle">
                <% function printTutors(tutors) {
                    let tutorString = ""
                    tutors.forEach((tutor, idx) => {
                        tutorString += tutor.tutor_name;
                        if (idx == course.tutors.length - 1){
                            return tutorString;
                        }
                        tutorString += ', ';
                    }); 
                }%>
                <%= printTutors(course.tutors) %>
            </p>

            <% function displayPrice(position, cost, symbol){
                if (cost == 0){
                    return "FREE";
                } else {
                    if (position == 1){
                        return symbol + cost;
                    }
                    else {
                        return cost + symbol;
                    }
                }
            } %>
            <p class="eds-course-price">
                <span>
                   <%= displayPrice(course.position, course.cost, course.currency_symbol) %>
                </span>
            <% if (Number(course.striked_cost) > 0){ %> 
                <span class="striked-price">
                    <%= displayPrice(course.position, course.striked_cost, course.currency_symbol) %>
                </span>
            <% } %> 
            </p>

        </div>
    </a>
<% }); %>`;

var COURSE_ON_SALE_COURSECARD = `
<% if(course){ %>
    <a class="eds-sale-card" href="<%= window.location.origin %>/course/<%= course.pretty_name %>-<%= course.institution_bundle_id %>">
        <img src="<%= course.img_url ? course.img_url : "https://edmingle.b-cdn.net/edmingle_websitebuilder/course-default.png" %>" alt="sale-image">
        <div>
            <h3 class="eds-sale-title"><%= course.bundle_name %></h3>
            <p class="eds-sale-subtitle ui-small-italic">
                <% function printTutors(tutors) {
                    let tutorString = ""
                    tutors.forEach((tutor, idx) => {
                        console.log(tutor.tutor_name, idx);
                        tutorString += tutor.tutor_name;
                        if (idx >= course.tutors.length - 1){
                            console.log('here');
                            return tutorString;
                        }
                        tutorString += ', ';
                        console.log(tutorString);
                    });
                    return tutorString;
                } %>
                
                <%= printTutors(course.tutors) %>
            </p>
            <p class="eds-sale-para ui-main subheading-gray"><%= course.bundle_description %></p>
            <div class="eds-sale-time">
                <% if (course.student_count > 0) { %>
                    <div style="display: flex;">
                        <svg width="20" height="21" viewBox="0 0 20 21" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M11 10.7983C11.5304 10.7983 12.0391 11.0091 12.4142 11.3841C12.7893 11.7592 13 12.2679 13 12.7983V14.2983C13 16.3523 10.912 17.7983 7.5 17.7983C4.088 17.7983 2 16.3523 2 14.2983V12.7983C2 12.2679 2.21071 11.7592 2.58579 11.3841C2.96086 11.0091 3.46957 10.7983 4 10.7983H11ZM16 10.7983C16.5304 10.7983 17.0391 11.0091 17.4142 11.3841C17.7893 11.7592 18 12.2679 18 12.7983V13.2983C18 15.3873 16.432 16.7983 13.5 16.7983C13.359 16.7983 13.22 16.7953 13.086 16.7883C13.625 16.1613 13.944 15.4023 13.993 14.5373L14 14.2983V12.7983C14 12.1063 13.766 11.4683 13.372 10.9613L13.235 10.7983H16ZM7.5 2.79834C7.95963 2.79834 8.41475 2.88887 8.83939 3.06476C9.26403 3.24065 9.64987 3.49846 9.97487 3.82347C10.2999 4.14847 10.5577 4.53431 10.7336 4.95895C10.9095 5.38359 11 5.83871 11 6.29834C11 6.75797 10.9095 7.21309 10.7336 7.63773C10.5577 8.06237 10.2999 8.44821 9.97487 8.77321C9.64987 9.09822 9.26403 9.35603 8.83939 9.53192C8.41475 9.70781 7.95963 9.79834 7.5 9.79834C6.57174 9.79834 5.6815 9.42959 5.02513 8.77321C4.36875 8.11684 4 7.2266 4 6.29834C4 5.37008 4.36875 4.47984 5.02513 3.82347C5.6815 3.16709 6.57174 2.79834 7.5 2.79834ZM14.5 4.79834C15.163 4.79834 15.7989 5.06173 16.2678 5.53057C16.7366 5.99941 17 6.6353 17 7.29834C17 7.96138 16.7366 8.59727 16.2678 9.06611C15.7989 9.53495 15.163 9.79834 14.5 9.79834C13.837 9.79834 13.2011 9.53495 12.7322 9.06611C12.2634 8.59727 12 7.96138 12 7.29834C12 6.6353 12.2634 5.99941 12.7322 5.53057C13.2011 5.06173 13.837 4.79834 14.5 4.79834Z"
                                fill="#858585" />
                        </svg>
                        <span class="ui-main" style="margin-left: 6px"><%= course.student_count %> </span>
                    </div>
                <% } %>
                <% if (course.rating > 0) { %> 
                <div class="eds-sale-rating">
                    <span><%= course.rating %></span>
                    <% let Rating = course.rating; %>
                    <% while(Rating > 0){ %>
                        <% if (Rating >= 1) { %> 
                            <svg width="18" height="17" viewBox="0 0 18 17" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M9.00021 0.625L6.44084 5.81125L0.720215 6.63812L4.86022 10.6769L3.88146 16.375L9.00021 13.6862L14.119 16.375L13.1402 10.6769L17.2802 6.64375L11.5596 5.81125L9.00021 0.625Z"
                                    fill="url(#paint0_linear_254_607)" />
                                <defs>
                                    <linearGradient id="paint0_linear_254_607" x1="0.720215" y1="8.5"
                                        x2="17.2802" y2="8.5" gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#A270F5" />
                                        <stop offset="1" stop-color="#7A32F0" />
                                    </linearGradient>
                                </defs>
                            </svg>
                    <% } else { %> 
                        <svg width="9" height="17" viewBox="0 0 9 17" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M6.44084 5.81125L0.720215 6.64375L4.86022 10.6769L3.88147 16.375L9.00022 13.6862V0.625L6.44084 5.81125Z"
                                fill="url(#paint0_linear_254_1011)" />
                            <defs>
                                <linearGradient id="paint0_linear_254_1011" x1="0.720215" y1="8.5"
                                    x2="9.00022" y2="8.5" gradientUnits="userSpaceOnUse">
                                    <stop stop-color="#A270F5" />
                                    <stop offset="1" stop-color="#7A32F0" />
                                </linearGradient>
                            </defs>
                        </svg>
                    <% } %>
                    <% Rating -= 1; %>        
                <% } %>
                    <span class="count ui-main">(<%= course.num_of_rating; %>)</span>
                </div>
                <% } %>
            </div>
            <div class="eds-sale-price">
                <% function displayPrice(position, cost, symbol){
                    if (cost == 0){
                        return "FREE";
                    } else {
                        if (position == 1){
                            return symbol + cost;
                        }
                        else {
                            return cost + symbol;
                        }
                    }
                } %>
                <span>
                    <%= displayPrice(course.position, course.cost, course.currency_symbol) %>
                </span>
                <% if (Number(course.striked_cost) > 0){ %>
                    <span class="striked-price">
                        <%= displayPrice(course.position, course.striked_cost, course.currency_symbol) %>
                    </span>
                <% } %> 
                </div>
            </div>
        </a>
    <% } %>`;

var COURSE_ON_SALE_PROMO = `
<% if(promo){ %>
    <div class="sale-container">
        <div class="sale-countdown">
            <div class="counter">
                <span class="days" id="day">0</span>
                <div class="ui-tiny body-white">Days</div>
            </div>
            <h3>
                :
            </h3>
            <div class="counter">
                <span class="hours" id="hour">0</span>
                <div class="ui-tiny body-white">Hours</div>
            </div>
            <h3>
                :
            </h3>
            <div class="counter">
                <span class="minutes" id="minute">0</span>
                <div class="ui-tiny body-white">Minutes</div>
            </div>
            <h3>
                :
            </h3>
            <div class="counter">
                <span class="seconds" id="second">0</span>
                <div class="ui-tiny body-white">Seconds</div>
            </div>
        </div>
        <div class="sale-coupon">
            <div class="discount">
                <span class="ui-small body-gray">Avail</span>
                <h3><%= promo.discount %>% OFF</h3>
            </div>
            <div class="coupon">
                <span class="ui-small body-gray">use this coupon code</span>
                <h3><%= promo.promo_code_text %></h3>
            </div>
        </div>
    </div>
<% } %>`;

var COURSE_DETAILS_LINK = `<%= window.location.origin %>/course/<%= course.pretty_name %>-<%= course.institution_bundle_id %>`;


var ALL_CATEGORY_COURSES_LISTING = `<% categories.forEach((category) => { %>
    <div class="category-wise-course-container">
        <a href="<%= window.location.origin %>/courses?categories_ids=<%= category.stream_id %>" class="category-label-link text-decoration-none">
            <span class="category-label"><%= category.stream_name %></span>
        </a>
        <% category.courses.forEach(course => { %>
            <section class="eds-top-category">
                <a class="eds-course-card"
                    href="<%= window.location.origin %>/course/<%= course.pretty_name %>-<%= course.institution_bundle_id %>"
                    data-noedit="">
                    <img src="<%= course.img_url ? course.img_url : "https://edmingle.b-cdn.net/edmingle_websitebuilder/course-default.png" %>" alt="course-image">
                    <div>
                        <h3 class="eds-course-title ui-big-bold subheading-gray">Edmingle’s Math
                            Masters
                            <%= course.bundle_name %>
                        </h3>
                        <% if (course.rating> 0) { %>
                            <div class="eds-course-rating">
                                <span class="ui-main">
                                    <%= course.rating %>
                                </span>
                                <% let Rating=course.rating; %>
                                    <% while(Rating> 0){ %>
                                        <% if (Rating>= 1) { %>
                                            <svg width="18" height="17" viewBox="0 0 18 17"
                                                fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M9.00021 0.625L6.44084 5.81125L0.720215 6.63812L4.86022 10.6769L3.88146 16.375L9.00021 13.6862L14.119 16.375L13.1402 10.6769L17.2802 6.64375L11.5596 5.81125L9.00021 0.625Z"
                                                    fill="url(#paint0_linear_254_607)" />
                                                <defs>
                                                    <linearGradient id="paint0_linear_254_607"
                                                        x1="0.720215" y1="8.5" x2="17.2802" y2="8.5"
                                                        gradientUnits="userSpaceOnUse">
                                                        <stop stop-color="#A270F5" />
                                                        <stop offset="1" stop-color="#7A32F0" />
                                                    </linearGradient>
                                                </defs>
                                            </svg>
                                            <% } else { %>
                                                <svg width="9" height="17" viewBox="0 0 9 17"
                                                    fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M6.44084 5.81125L0.720215 6.64375L4.86022 10.6769L3.88147 16.375L9.00022 13.6862V0.625L6.44084 5.81125Z"
                                                        fill="url(#paint0_linear_254_1011)" />
                                                    <defs>
                                                        <linearGradient id="paint0_linear_254_1011"
                                                            x1="0.720215" y1="8.5" x2="9.00022"
                                                            y2="8.5" gradientUnits="userSpaceOnUse">
                                                            <stop stop-color="#A270F5" />
                                                            <stop offset="1" stop-color="#7A32F0" />
                                                        </linearGradient>
                                                    </defs>
                                                </svg>
                                                <% } %>
                                                    <% Rating -=1; %>
                                                        <% } %>
                                                            <span class="count ui-main">(<%=
                                                                    course.num_of_rating; %>)</span>
                            </div>
                            <% } %>
                                <p class="ui-tiny-italic body-gray eds-course-subtitle">Kirill
                                    Eremenko, Hadelin
                                    <% function printTutors(tutors) { let tutorString=""
                                        tutors.forEach((tutor, idx)=> {
                                        tutorString += tutor.tutor_name;
                                        if (idx == course.tutors.length - 1){
                                        return tutorString;
                                        }
                                        tutorString += ', ';
                                        });
                                        }%>
                                        <%= printTutors(course.tutors) %>
                                </p>
                                <% function displayPrice(position, cost, symbol){ if (cost==0){
                                    return "FREE" ; } else { if (position==1){ return symbol + cost;
                                    } else { return cost + symbol; } } } %>
                                    <p class="eds-course-price">
                                        <span>
                                            <%= displayPrice(course.position, course.cost,
                                                course.currency_symbol) %>
                                        </span>
                                        <% if (Number(course.striked_cost)> 0){ %>
                                            <span class="striked-price">
                                                <%= displayPrice(course.position,
                                                    course.striked_cost, course.currency_symbol) %>
                                            </span>
                                            <% } %>
                                    </p>
                    </div>
                </a>
                <% }); %>
            </section>
            <div class="see-all-courses-link-container">
                <a href="<%= window.location.origin %>/courses?categories_ids=<%= category.stream_id %>"
                    class="see-all-courses-link text-decoration-none">
                    <span class="see-all-courses-link-items">See All Courses</span>
                    <span class="see-all-courses-link-items"><svg width="15" height="15"
                            viewBox="0 0 10 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M9 1.5L1 9.5" stroke="white" stroke-opacity="0.9"
                                stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                            <path d="M2.33301 1.5H8.99967V8.16667" stroke="white"
                                stroke-opacity="0.9" stroke-width="1.5" stroke-linecap="round"
                                stroke-linejoin="round" />
                        </svg>
                    </span>
                </a>
            </div>
    </div>
<% }); %>`;


var PRICING_OPTIONS_LISTING = `<% if (freeEnrollAllowed){ %> 
    <div class="pricing-card" style="border: 1px solid var(--light-body)">
        <h3 class="price-title ui-large accent-gray">Enroll For Free</h3>
        <p class="price ui-small body-gray">
            You can enroll now for free and access all the demo content. Check if you like our
            course.
        </p>
    </div>
<% } %>

<% feeTemplates.forEach(template => { %> 
    <div class="pricing-card light bg-secondary-gradient">
        <h3 class="price-title ui-large"> <%= template.template_name %> </h3>
        <p class="price">
        <% function displayPrice(position, cost, symbol){
            if (cost == 0){
                return "FREE";
            } else {
                console.log(template);
                if (position == 1){
                    return symbol + cost;
                }
                else {
                    return cost + symbol;
                }
            }
        } %> 
            <span> <%= displayPrice(template.currencydetails.default_postition, cost, template.currencydetails.currency_symbol) %></span>
            <% if(striked_cost > 0){ %>
                &nbsp;
                <span class="striked-price"><%= displayPrice(template.currencydetails.default_postition, striked_cost, template.currencydetails.currency_symbol) %></span>
            <% } %>
        </p>
        <div class="price-description">
            <p class="ui-small">
                Validity
            </p>
            <div class="price-expiry">
                <svg width="17" height="19" viewBox="0 0 17 19" fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                    style="fill: rgba(var(--classic-primary),1);">
                    <path
                        d="M3.77778 8.55H5.66667V10.45H3.77778V8.55ZM17 3.8V17.1C17 18.145 16.15 19 15.1111 19H1.88889C1.38792 19 0.907478 18.7998 0.553243 18.4435C0.199007 18.0872 0 17.6039 0 17.1L0.00944444 3.8C0.00944444 2.755 0.840555 1.9 1.88889 1.9H2.83333V0H4.72222V1.9H12.2778V0H14.1667V1.9H15.1111C16.15 1.9 17 2.755 17 3.8ZM1.88889 5.7H15.1111V3.8H1.88889V5.7ZM15.1111 17.1V7.6H1.88889V17.1H15.1111ZM11.3333 10.45H13.2222V8.55H11.3333V10.45ZM7.55556 10.45H9.44444V8.55H7.55556V10.45Z">
                    </path>
                </svg>
                <% if (template.expiration_after > 0){ %>
                    <span class="ui-main-bold"><%= template.expiration_after ? (template.expiration_after == 1 ? template.expiration_after + " Day" : template.expiration_after + " Days") : null %> Access</span>
                <% } else { %>
                    <span class="ui-main-bold">Lifetime Access</span>
                <% } %>
            </div>
            <a onclick="window.renderInstallmentDetails(this)" class="ui-small installment-schedule" templates-data="<%= encodeURIComponent(JSON.stringify(template.instalments)) %>">
                View installments schedule
            </a>
        </div>
    </div>
<% }); %>`;

var REFER_N_EARN = `
<% if(refObj){ %>
    <div class="col-md-6" style="display: flex; flex-direction: column; justify-content: center;">
    <div style="max-width:500px">
        <h1 class="section-heading heading-white">Refer &amp; Earn </h1>
        <% if (localStorage.getItem('apikey')){ %>
            <p class="ui-main body-white">
                Share your referral code with friends &amp; earn credits.
            </p>
            <hr style="margin:10px 0;background: white; padding:0.5px; width: 100px;">
            <p class="ui-big-bold subheading-white">
                Share via
            </p>
            <div class="share-links">
                <ul style="list-style:none;">
                    <li>
                        <a id="whatsapp-share" onclick="window.shareLink(this.id, '<%= ref_code %>')">
                            <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M25.4426 6.76847C21.4173 0.545693 13.199 -1.30432 6.82568 2.56389C0.620052 6.4321 -1.39258 14.8413 2.63269 21.064L2.96813 21.5686L1.62637 26.6141L6.65796 25.2686L7.16112 25.605C9.34147 26.7823 11.6895 27.455 14.0376 27.455C16.5534 27.455 19.0692 26.7823 21.2496 25.4368C27.4552 21.4004 29.3001 13.1594 25.4426 6.76847ZM21.9204 19.7186C21.2496 20.7277 20.411 21.4004 19.2369 21.5686C18.5661 21.5686 17.7275 21.9049 14.3731 20.5595C11.5218 19.214 9.17375 17.0276 7.49656 14.5049C6.49024 13.3276 5.98708 11.814 5.81936 10.3003C5.81936 8.95485 6.32252 7.77757 7.16112 6.93665C7.49656 6.60029 7.832 6.4321 8.16744 6.4321H9.00603C9.34147 6.4321 9.67691 6.4321 9.84463 7.10483C10.1801 7.94575 11.0187 9.96395 11.0187 10.1321C11.1864 10.3003 11.1864 10.6367 11.0187 10.8049C11.1864 11.1412 11.0187 11.4776 10.851 11.6458C10.6832 11.814 10.5155 12.1503 10.3478 12.3185C10.0124 12.4867 9.84463 12.8231 10.0124 13.1594C10.6832 14.1685 11.5218 15.1776 12.3604 16.0185C13.3667 16.8595 14.3731 17.5322 15.5471 18.0367C15.8825 18.2049 16.218 18.2049 16.3857 17.8686C16.5534 17.5322 17.392 16.6913 17.7275 16.3549C18.0629 16.0185 18.2306 16.0185 18.5661 16.1867L21.2496 17.5322C21.585 17.7004 21.9204 17.8686 22.0882 18.0367C22.2559 18.5413 22.2559 19.214 21.9204 19.7186Z" fill="white"></path>
                            </svg>
                        </a>
                        <p class="subheading-white">
                            Whatsapp
                        </p>
                    </li>
                    <li>
                        <a id="facebook-share" onclick="window.shareLink(this.id, '<%= ref_code %>')">
                            <svg width="27" height="27" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M0.359375 13.7156C0.359375 20.319 5.1553 25.81 11.4278 26.9235V17.3305H8.10724V13.6414H11.4278V10.6895C11.4278 7.36898 13.5673 5.52499 16.5934 5.52499C17.5519 5.52499 18.5857 5.6722 19.5442 5.81941V9.21408H17.8474C16.2237 9.21408 15.8551 10.0254 15.8551 11.0592V13.6414H19.397L18.807 17.3305H15.8551V26.9235C22.1276 25.81 26.9235 20.3201 26.9235 13.7156C26.9235 6.3695 20.9466 0.359375 13.6414 0.359375C6.3363 0.359375 0.359375 6.3695 0.359375 13.7156Z" fill="white"></path>
                            </svg>
                        </a>
                        <p class="subheading-white">
                            Facebook
                        </p>
                    </li>
                    <li>
                        <a id="manual-share" onclick="window.shareLink(this.id, '<%= ref_code %>')">
                            <svg width="24" height="28" viewBox="0 0 24 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M18.2857 17.3641C17.1113 17.3641 16.0318 17.775 15.1797 18.4623L9.85579 15.0958C10.0135 14.3742 10.0135 13.6266 9.85579 12.9051L15.1797 9.53858C16.0318 10.2259 17.1113 10.6368 18.2857 10.6368C21.0399 10.6368 23.2727 8.37786 23.2727 5.59135C23.2727 2.80484 21.0399 0.545898 18.2857 0.545898C15.5315 0.545898 13.2987 2.80484 13.2987 5.59135C13.2987 5.96755 13.3397 6.33398 13.4169 6.68669L8.09299 10.0532C7.24088 9.36593 6.16145 8.95499 4.98701 8.95499C2.23278 8.95499 0 11.2139 0 14.0004C0 16.787 2.23278 19.0459 4.98701 19.0459C6.16145 19.0459 7.24088 18.635 8.09299 17.9477L13.4169 21.3142C13.3382 21.6739 13.2986 22.0412 13.2987 22.4095C13.2987 25.196 15.5315 27.455 18.2857 27.455C21.0399 27.455 23.2727 25.196 23.2727 22.4095C23.2727 19.623 21.0399 17.3641 18.2857 17.3641Z" fill="white"></path>
                            </svg>
                        </a>
                        <p class="subheading-white">
                            Share
                        </p>
                    </li>
                    <li>
                        <div class="copycode" onclick="window.copyToClipboard(this.innerHTML)">
                            <%= ref_code %>
                        </div>
                        <p class="subheading-white">
                            Tap to Copy
                        </p>
                    </li>
                </ul>
            </div>
        <% } else { %>
            <p class="ui-main body-white">
                SignUp to get your own referral code. Share your code with your friends and earn rewards.
            </p>
            <hr style="margin:10px 0;background: white; padding:0.5px; width: 100px;">
            <a href="<%= window.location.origin %>/register" class="eds-btn white" style="display: inline-block; margin:16px 0px 20px 0px;">
                Sign up
            </a>
        <% } %>
    </div>
    </div>
    <div class="col-md-6">
    <section class="eds-refer-section">
        <h2 class="ui-big-bold subheading-white">
            How it works
        </h2>
        <div class="eds-refer-card">
            <div class="refer-image">
                <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M22.8437 1.03415L1.66255 7.00833C1.48435 7.05859 1.32575 7.16204 1.20792 7.30485C1.0901 7.44767 1.01867 7.62304 1.00319 7.80754C0.987702 7.99203 1.0289 8.17686 1.12127 8.33731C1.21364 8.49777 1.35279 8.62621 1.52011 8.70547L11.2521 13.3154C11.4419 13.4053 11.5947 13.5581 11.6847 13.7479L16.2945 23.4799C16.3738 23.6472 16.5022 23.7864 16.6627 23.8787C16.8231 23.9711 17.008 24.0123 17.1925 23.9968C17.377 23.9813 17.5523 23.9099 17.6951 23.7921C17.838 23.6743 17.9414 23.5156 17.9917 23.3375L23.9659 2.15629C24.0097 2.0007 24.0114 1.83623 23.9705 1.6798C23.9297 1.52338 23.8479 1.38067 23.7336 1.26636C23.6193 1.15205 23.4766 1.07028 23.3202 1.02946C23.1638 0.988648 22.9993 0.990265 22.8437 1.03415Z" stroke="#333333" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                </svg>
            </div>
            <div class="refer-text">
                <h3 class="ui-big-bold subheading-white">Invite your friends</h3>
                <p class="ui-small body-white">Invite your friends</p>
            </div>
        </div>
        <div class="eds-refer-card">
            <div class="refer-image">
                <svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6.15573 11.4919C5.42516 11.4919 4.711 11.2752 4.10356 10.8693C3.49611 10.4635 3.02267 9.88657 2.74309 9.21161C2.46351 8.53666 2.39036 7.79395 2.53289 7.07742C2.67542 6.36089 3.02722 5.70272 3.54381 5.18613C4.0604 4.66954 4.71857 4.31774 5.4351 4.17521C6.15163 4.03269 6.89433 4.10584 7.56929 4.38541C8.24425 4.66499 8.82114 5.13843 9.22702 5.74588C9.6329 6.35332 9.84954 7.06748 9.84954 7.79805C9.84737 8.77705 9.45751 9.71532 8.76525 10.4076C8.073 11.0998 7.13472 11.4897 6.15573 11.4919ZM6.15573 5.74593C5.74986 5.74593 5.3531 5.86629 5.01563 6.09178C4.67816 6.31727 4.41514 6.63776 4.25982 7.01274C4.1045 7.38772 4.06386 7.80033 4.14304 8.1984C4.22222 8.59647 4.41767 8.96212 4.70466 9.24912C4.99166 9.53611 5.35731 9.73156 5.75538 9.81074C6.15345 9.88992 6.56606 9.84928 6.94104 9.69396C7.31602 9.53864 7.63651 9.27562 7.862 8.93815C8.08749 8.60068 8.20785 8.20392 8.20785 7.79805C8.20785 7.52856 8.15477 7.26171 8.05164 7.01274C7.94851 6.76377 7.79735 6.53754 7.6068 6.34698C7.41624 6.15643 7.19001 6.00527 6.94104 5.90214C6.69207 5.79901 6.42522 5.74593 6.15573 5.74593ZM15.185 9.02932C14.2921 9.02932 13.4193 8.76454 12.6768 8.26847C11.9344 7.77239 11.3558 7.06729 11.014 6.24235C10.6723 5.4174 10.5829 4.50965 10.7571 3.6339C10.9313 2.75814 11.3613 1.9537 11.9927 1.32232C12.6241 0.690929 13.4285 0.260949 14.3043 0.08675C15.18 -0.0874492 16.0878 0.00195615 16.9127 0.34366C17.7377 0.685364 18.4428 1.26402 18.9389 2.00645C19.4349 2.74888 19.6997 3.62175 19.6997 4.51466C19.6975 5.71136 19.2212 6.85842 18.375 7.70461C17.5288 8.55081 16.3817 9.02715 15.185 9.02932ZM15.185 1.6417C14.6168 1.6417 14.0614 1.81019 13.5889 2.12588C13.1165 2.44156 12.7482 2.89026 12.5308 3.41523C12.3133 3.94019 12.2564 4.51785 12.3673 5.07515C12.4781 5.63245 12.7518 6.14436 13.1536 6.54615C13.5553 6.94795 14.0673 7.22157 14.6246 7.33242C15.1819 7.44328 15.7595 7.38638 16.2845 7.16894C16.8095 6.95149 17.2581 6.58325 17.5738 6.1108C17.8895 5.63834 18.058 5.08288 18.058 4.51466C18.058 3.7527 17.7553 3.02195 17.2165 2.48317C16.6778 1.94438 15.947 1.6417 15.185 1.6417ZM22.1623 22.1629H8.20785C7.99014 22.1629 7.78136 22.0764 7.62742 21.9225C7.47348 21.7685 7.387 21.5597 7.387 21.342C7.387 21.1243 7.47348 20.9155 7.62742 20.7616C7.78136 20.6077 7.99014 20.5212 8.20785 20.5212H21.3414V15.9409C21.3392 14.7442 20.8629 13.5971 20.0167 12.7509C19.1705 11.9047 18.0234 11.4284 16.8267 11.4262H13.5434C12.6743 11.4299 11.8247 11.6843 11.0967 12.159C10.3686 12.6337 9.79307 13.3084 9.43912 14.1022C9.39947 14.207 9.33879 14.3026 9.2608 14.383C9.18281 14.4635 9.08918 14.5271 8.98566 14.57C8.88214 14.6129 8.77094 14.6342 8.65889 14.6325C8.54685 14.6308 8.43634 14.6062 8.33417 14.5601C8.23201 14.5141 8.14035 14.4476 8.06484 14.3648C7.98933 14.282 7.93158 14.1847 7.89514 14.0787C7.85869 13.9727 7.84433 13.8604 7.85294 13.7487C7.86154 13.637 7.89293 13.5282 7.94518 13.4291C8.42806 12.3435 9.21555 11.4213 10.2121 10.7744C11.2087 10.1275 12.3716 9.78361 13.5598 9.7845H16.8432C18.4759 9.7845 20.0418 10.4331 21.1964 11.5877C22.3509 12.7422 22.9995 14.3081 22.9995 15.9409V21.342C22.9995 21.4512 22.9778 21.5593 22.9355 21.66C22.8932 21.7607 22.8313 21.8519 22.7533 21.9283C22.6754 22.0048 22.5829 22.0649 22.4814 22.1052C22.3799 22.1454 22.2714 22.1651 22.1623 22.1629Z" fill="#333333"></path>
                    <path d="M11.4919 22.1627H0.820847C0.603145 22.1627 0.394359 22.0762 0.240421 21.9222C0.0864818 21.7683 0 21.5595 0 21.3418V18.4689C0 16.8361 0.648614 15.2702 1.80315 14.1157C2.9577 12.9611 4.52359 12.3125 6.15635 12.3125C7.78912 12.3125 9.35501 12.9611 10.5096 14.1157C11.6641 15.2702 12.3127 16.8361 12.3127 18.4689V21.3418C12.3127 21.5595 12.2262 21.7683 12.0723 21.9222C11.9184 22.0762 11.7096 22.1627 11.4919 22.1627ZM1.64169 20.521H10.671V18.4689C10.671 17.2715 10.1954 16.1232 9.3487 15.2765C8.50204 14.4298 7.35372 13.9542 6.15635 13.9542C4.95899 13.9542 3.81067 14.4298 2.96401 15.2765C2.11735 16.1232 1.64169 17.2715 1.64169 18.4689V20.521Z" fill="#333333"></path>
                    <path d="M4.10454 19.6995C3.88684 19.6995 3.67805 19.613 3.52411 19.4591C3.37017 19.3051 3.28369 19.0963 3.28369 18.8786V17.2369C3.28369 17.0192 3.37017 16.8105 3.52411 16.6565C3.67805 16.5026 3.88684 16.4161 4.10454 16.4161C4.32224 16.4161 4.53103 16.5026 4.68497 16.6565C4.8389 16.8105 4.92539 17.0192 4.92539 17.2369V18.8786C4.92539 19.0963 4.8389 19.3051 4.68497 19.4591C4.53103 19.613 4.32224 19.6995 4.10454 19.6995ZM18.8798 19.5764C18.6621 19.5764 18.4533 19.4899 18.2994 19.3359C18.1454 19.182 18.0589 18.9732 18.0589 18.7555V17.1138C18.0589 16.8961 18.1454 16.6873 18.2994 16.5334C18.4533 16.3795 18.6621 16.293 18.8798 16.293C19.0975 16.293 19.3063 16.3795 19.4602 16.5334C19.6142 16.6873 19.7006 16.8961 19.7006 17.1138V18.7555C19.7006 18.9732 19.6142 19.182 19.4602 19.3359C19.3063 19.4899 19.0975 19.5764 18.8798 19.5764Z" fill="#333333"></path>
                </svg>
            </div>
            <div class="refer-text">
                <h3 class="ui-big-bold subheading-white">You get ₹<%= refObj.referral_joins %> and your friend gets ₹<%= refObj.referral_signup %></h3>
                <p class="ui-small body-white">When your friend sign’s up.</p>
            </div>
        </div>
        <div class="eds-refer-card">
            <div class="refer-image">
                <svg width="23" height="22" viewBox="0 0 23 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M20.2052 10.9134C19.9846 10.9134 19.773 10.8258 19.617 10.6698C19.461 10.5138 19.3734 10.3022 19.3734 10.0816V7.68588C19.4046 7.21449 19.2489 6.74977 18.9401 6.39232C18.6312 6.03486 18.1939 5.81344 17.723 5.77598H3.32891C3.10829 5.77598 2.89671 5.68834 2.74071 5.53234C2.58471 5.37634 2.49707 5.16476 2.49707 4.94414C2.49707 4.72352 2.58471 4.51194 2.74071 4.35594C2.89671 4.19994 3.10829 4.1123 3.32891 4.1123H17.723C18.6368 4.14527 19.5003 4.53928 20.124 5.20794C20.7478 5.87659 21.0809 6.76531 21.0504 7.67922V10.0749C21.0513 10.1858 21.03 10.2958 20.9877 10.3984C20.9455 10.501 20.8832 10.5941 20.8044 10.6722C20.7257 10.7503 20.6321 10.8119 20.5292 10.8533C20.4263 10.8948 20.3161 10.9152 20.2052 10.9134Z" fill="#333333"></path>
                    <path d="M17.7234 21.5348H3.32928C2.41545 21.5018 1.552 21.1078 0.928253 20.4392C0.304507 19.7705 -0.0286167 18.8818 0.00192975 17.9679V2.95488C0.00192975 2.73427 0.0895695 2.52269 0.245569 2.36669C0.401569 2.21069 0.61315 2.12305 0.833767 2.12305C1.05438 2.12305 1.26596 2.21069 1.42196 2.36669C1.57796 2.52269 1.6656 2.73427 1.6656 2.95488V17.9679C1.63443 18.4386 1.79031 18.9025 2.09935 19.2589C2.40839 19.6153 2.84561 19.8353 3.31597 19.8711H17.7234C18.1937 19.8353 18.631 19.6153 18.94 19.2589C19.249 18.9025 19.4049 18.4386 19.3737 17.9679V16.2776C19.3737 16.057 19.4614 15.8454 19.6174 15.6894C19.7734 15.5334 19.985 15.4457 20.2056 15.4457C20.4262 15.4457 20.6378 15.5334 20.7938 15.6894C20.9498 15.8454 21.0374 16.057 21.0374 16.2776V17.9679C21.068 18.8796 20.7367 19.7663 20.1158 20.4346C19.4949 21.1029 18.6349 21.4984 17.7234 21.5348Z" fill="#333333"></path>
                    <path d="M17.3834 5.77698C17.1651 5.77701 16.9555 5.6912 16.7999 5.53807C16.6442 5.38493 16.5551 5.17676 16.5516 4.95846V3.1151C16.536 2.72027 16.3649 2.34762 16.0757 2.07837C15.7865 1.80911 15.4026 1.66509 15.0077 1.67769H3.02922C2.68964 1.66508 2.3589 1.78757 2.10944 2.01832C1.85998 2.24907 1.71214 2.56927 1.69829 2.90881C1.71214 3.24834 1.85998 3.56855 2.10944 3.7993C2.3589 4.03005 2.68964 4.15253 3.02922 4.13993H10.3494C10.57 4.13993 10.7816 4.22757 10.9376 4.38357C11.0936 4.53957 11.1812 4.75115 11.1812 4.97176C11.1812 5.19238 11.0936 5.40396 10.9376 5.55996C10.7816 5.71596 10.57 5.8036 10.3494 5.8036H3.00926C2.62156 5.80976 2.23644 5.73938 1.87598 5.59648C1.51552 5.45358 1.18679 5.24098 0.908613 4.97084C0.630441 4.7007 0.408293 4.37834 0.254892 4.02222C0.101491 3.6661 0.0198512 3.28322 0.0146484 2.8955C0.0286503 2.11476 0.351829 1.37148 0.913258 0.828762C1.47469 0.286047 2.2285 -0.0117565 3.00926 0.000707647H14.9877C15.4045 -0.00811254 15.8189 0.0655948 16.2071 0.217586C16.5953 0.369577 16.9495 0.59685 17.2495 0.88632C17.5495 1.17579 17.7893 1.52174 17.955 1.90425C18.1207 2.28676 18.2092 2.69827 18.2152 3.1151V4.95846C18.2117 5.17676 18.1226 5.38493 17.9669 5.53807C17.8113 5.6912 17.6017 5.77701 17.3834 5.77698ZM20.744 17.09H16.1323C15.2315 17.1078 14.3605 16.7674 13.7104 16.1436C13.0603 15.5197 12.6844 14.6634 12.6652 13.7626V12.5581C12.6844 11.6573 13.0603 10.801 13.7104 10.1772C14.3605 9.55331 15.2315 9.21293 16.1323 9.23077H20.7906C21.0816 9.23164 21.3696 9.28982 21.6381 9.402C21.9067 9.51417 22.1505 9.67813 22.3556 9.88453C22.5608 10.0909 22.7233 10.3357 22.8338 10.6049C22.9444 10.8741 23.0008 11.1624 23 11.4534V14.9072C22.9895 15.4956 22.7461 16.0557 22.3232 16.4649C21.9003 16.8741 21.3324 17.0989 20.744 17.09ZM16.1323 10.9011C15.6738 10.883 15.2268 11.0472 14.8888 11.3577C14.5509 11.6681 14.3496 12.0997 14.3289 12.5581V13.7959C14.3496 14.2543 14.5509 14.6859 14.8888 14.9963C15.2268 15.3068 15.6738 15.471 16.1323 15.4529H20.744C20.8894 15.4529 21.0289 15.3956 21.1322 15.2935C21.2356 15.1913 21.2946 15.0526 21.2964 14.9072V11.4534C21.2924 11.3206 21.2376 11.1944 21.1433 11.1007C21.0934 11.0486 21.0335 11.007 20.9672 10.9784C20.9009 10.9498 20.8295 10.9348 20.7573 10.9344L16.1323 10.9011Z" fill="#333333"></path>
                    <path d="M16.5916 14.0084C17.051 14.0084 17.4234 13.636 17.4234 13.1766C17.4234 12.7172 17.051 12.3447 16.5916 12.3447C16.1322 12.3447 15.7598 12.7172 15.7598 13.1766C15.7598 13.636 16.1322 14.0084 16.5916 14.0084Z" fill="#333333"></path>
                </svg>
            </div>
            <div class="refer-text">
                <h3 class="ui-big-bold subheading-white">Your wallet credits can be used</h3>
                <p class="ui-small body-white">To pay course fees or can be redeemed from the
                    institute.</p>
            </div>
        </div>
        <div class="eds-refer-card">
            <div class="refer-image">
                <svg width="23" height="22" viewBox="0 0 23 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0 7.89855C0 7.1364 0.617847 6.51855 1.38 6.51855H21.62C22.3822 6.51855 23 7.1364 23 7.89855H22.0367L20.8756 18.0905C20.7644 18.9182 20.3572 19.6776 19.7293 20.2281C19.1014 20.7787 18.2953 21.0832 17.4602 21.0853H5.53977C4.70469 21.0832 3.89863 20.7787 3.27076 20.2281C2.64289 19.6776 2.23571 18.9182 2.12458 18.0905L0.963573 7.89855H0ZM3.49268 17.9102C3.55928 18.4063 3.80335 18.8615 4.1797 19.1915C4.55605 19.5215 5.03921 19.704 5.53977 19.7053H17.4602C17.9608 19.7041 18.444 19.5215 18.8203 19.1915C19.1967 18.8615 19.4407 18.4063 19.5073 17.9102L20.6446 7.89855H2.35543L3.49268 17.9102Z" fill="#333333"></path>
                    <path d="M17.7885 2.96094C17.5909 2.0388 16.7759 1.38 15.8329 1.38H7.16633C6.22325 1.38 5.40833 2.0388 5.21072 2.96094L4.6456 5.5982H3.2334L4.00077 2.02007C4.25348 0.841745 5.29488 0 6.5 0H11.4996H16.1404C17.5548 0 18.7771 0.987942 19.0737 2.37092L19.7658 5.5982H18.3536L17.7885 2.96094Z" fill="#333333"></path>
                    <path d="M11.4552 15.7088C11.0647 16.0994 10.4315 16.0994 10.041 15.7088L8.48487 14.1526C8.21546 13.8831 8.21547 13.4463 8.48489 13.1769C8.75432 12.9075 9.19114 12.9075 9.46057 13.1769L10.7481 14.4644L13.5394 11.6734C13.8088 11.4039 14.2456 11.4039 14.515 11.6734C14.7845 11.9428 14.7845 12.3796 14.515 12.649L11.4552 15.7088Z" fill="#333333"></path>
                </svg>
            </div>
            <div class="refer-text">
                <h3 class="ui-big-bold subheading-white">You get ₹<%= refObj.referral_books_package %></h3>
                <p class="ui-small body-white">when your friend purchases the first course.</p>
            </div>
        </div>
    </section>
    </div>
<% } %>`;

var LOADING_STATE = `<div class="loading-container-client-side" style="width: 100%; height: 50vh; display: flex;">
        <div class="loader-client-side"></div>
    </div>`;

var COURSE_CARD_IMAGE = `<% function getCourseCardImageURL(course){
        return course.img_url ? course.img_url : "https://edmingle.b-cdn.net/edmingle_websitebuilder/course-default.png" %>;
    } %>`

templateSectionMapper = {
    'related-courses': RELATED_COURSE_LISTING,
    'all-coupons': COUPONS_LISTING,
    'category-courses': CATEGORY_COURSE_LISTING,
    'course-on-sale-course': COURSE_ON_SALE_COURSECARD,
    'course-on-sale-promo': COURSE_ON_SALE_PROMO,
    'pricing-options': PRICING_OPTIONS_LISTING,
    'refer-n-earn': REFER_N_EARN,
    'all-category-courses-listing': ALL_CATEGORY_COURSES_LISTING
    // 'demo-content': DEMO_CONTENT_LISTING,
}

class RenderEngineClient {
    constructor() {
        if (window.location.origin == 'http://localhost:3007' || window.location.origin.includes('file://')) {
            this.apiPath = `http://localhost/nuSource/api/v1/`
        } else {
            this.apiPath = `${window.location.origin}/nuSource/api/v1/`
        }

        this.apiCalls = {}
        this.intervals = []
        window.RenderEngineClient = this;
    }

    fetcher = (url, headers = {}) => {
        return new Promise(async (resolve, reject) => {
            try {
                if (url in this.apiCalls) {
                    resolve(await this.apiCalls[url]);
                }
                else {
                    console.log('called');
                    this.apiCalls[url] = axios.get(url, { headers: headers })
                    resolve(await this.apiCalls[url]);
                }
            }
            catch (error) {
                reject(error);
            }
            console.log(this.apiCalls);
        });
    };

    getCourseDetails = async (institution_bundle_id) => {
        let serviceUrl = this.apiPath + endpointMapper.getCourseDetails({ institution_bundle_id: institution_bundle_id });
        let response = await this.fetcher(serviceUrl);
        response = response.data;
        return response;
    }

    renderTemplate = async (identifier, templateVariables) => {
        let template = templateSectionMapper[identifier];
        console.log(identifier, template);
        return await ejs.render(template, templateVariables);
    }

    formCourseList = (courseList, maxLen, currId = -1) => {
        let courses = [];
        let formedCount = 0;
        for (let i = 0; i < courseList.length; i++) {
            let orgCourses = courseList[i].course_bundles;
            for (let j = 0; j < orgCourses.length; j++) {
                if (formedCount >= maxLen) return courses;
                if (orgCourses[j].institution_bundle_id == currId) continue;
                courses.push(orgCourses[j]);
                formedCount += 1;
            }
        }
        return courses;
    }

    addLoadingState = (container) => {
        container.innerHTML = LOADING_STATE;
    }

    relatedCourses = async (uid) => {
        try {
            let container = document.getElementById(`related-courses-rendering==${uid}`);
            this.addLoadingState(container);
            let template_dependencies = { relatedCourses: [] }
            let form = document.getElementById(`related-courses==${uid}`);
            console.log(form);
            let paramObj = {
                institution_id: Object.values(form)[0].value,
                institution_bundle_id: Object.values(form)[1].value
            }
            let currentCourse = await this.getCourseDetails(paramObj.institution_bundle_id);
            let tutorIds = "";
            currentCourse.tutors.map(tutor => tutorIds += `${tutor.tutor_id},`);
            paramObj.tutor_ids = tutorIds;
            let serviceUrl = this.apiPath + endpointMapper.getTutorCourses(paramObj);
            let response = await this.fetcher(serviceUrl);
            response = response.data;
            template_dependencies.relatedCourses = this.formCourseList(response.institute_courses, 8, paramObj.institution_bundle_id);
            let renderedTemplate = await this.renderTemplate('related-courses', template_dependencies);
            console.log(renderedTemplate);
            container.innerHTML = renderedTemplate;
        } catch (error) {
            console.log(error);
        }
    }

    allCoupons = async (uid) => {
        try {
            let container = document.getElementById(`all-coupons-rendering==${uid}`);
            this.addLoadingState(container);
            let template_dependencies = { coupons: [], course: null }
            let form = document.getElementById(`all-coupons==${uid}`);
            let paramObj = {
                institution_id: Object.values(form)[0].value,
                institution_bundle_id: Object.values(form)[1].value
            }
            // let coupons = [];
            if (paramObj.institution_bundle_id && paramObj.institution_bundle_id != "null") {
                let serviceUrl = this.apiPath + endpointMapper.getAllCourseCoupons(paramObj);
                let response = await this.fetcher(serviceUrl);
                response = response.data;
                console.log(this.apiCalls);
                template_dependencies.coupons = response.promocode;
                template_dependencies.course = response.bundle;
            } else {
                let serviceUrl = this.apiPath + endpointMapper.getAllCoupons(paramObj);
                let response = await this.fetcher(serviceUrl);
                console.log(response);
                response = response.data
                template_dependencies.coupons = response.coupons;
            }
            // console.log(template_dependencies);
            let renderedTemplate = await this.renderTemplate('all-coupons', template_dependencies);
            console.log(renderedTemplate);
            container.innerHTML = renderedTemplate;
        } catch (error) {
            console.log(error);
        }
    }

    topCategoryCourses = async (uid) => {
        try {
            let container = document.getElementById(`top-category-courses-rendering==${uid}`);
            this.addLoadingState(container);
            let template_dependencies = { courses: [] }
            let form = document.getElementById(`top-category-course==${uid}`);
            let paramObj = {
                institution_id: Object.values(form)[0].value,
                category_id: Object.values(form)[1].value
            }
            let serviceUrl = this.apiPath + endpointMapper.getCategoryCourses(paramObj);
            let response = await this.fetcher(serviceUrl);
            response = response.data;
            template_dependencies.courses = this.formCourseList(response.institute_courses, 8);
            console.log(template_dependencies);
            let renderedTemplate = await this.renderTemplate('category-courses', template_dependencies);
            document.getElementById(`category-all-courses-nav-link==${uid}`).href = window.location.origin + `/courses?categories_ids=${paramObj.category_id}`
            console.log(renderedTemplate);
            container.innerHTML = renderedTemplate;
        } catch (error) {
            console.log(error);
        }
    }

    courseOnSale = async (uid) => {
        try {
            this.intervals.forEach(intervalObj => clearInterval(intervalObj));
            let container1 = document.getElementById(`course-on-sale-course-render==${uid}`);
            let container2 = document.getElementById(`course-on-sale-promo-render==${uid}`);
            this.addLoadingState(container1);
            this.addLoadingState(container2)
            let template_dependencies = { course: [], promo: null };
            let form = document.getElementById(`course-on-sale==${uid}`);
            let paramObj = {
                institution_bundle_id: Object.values(form)[0].value,
                promo_code_id: Object.values(form)[1].value,
                timer: Object.values(form)[2].value
            }
            let serviceUrl = this.apiPath + endpointMapper.getCourseOnSaleData(paramObj);
            let response = await this.fetcher(serviceUrl);
            response = response.data;
            template_dependencies.course = response.course_details.bundle;
            template_dependencies.course.tutors = response.course_details.tutors;
            template_dependencies.promo = response.promo_code_details;
            let courseCardRendered = await this.renderTemplate('course-on-sale-course', template_dependencies);
            let promoCardRendered = await this.renderTemplate('course-on-sale-promo', template_dependencies);
            console.log(template_dependencies);
            container1.innerHTML = courseCardRendered;
            container2.innerHTML = promoCardRendered;
            let courseLinkElement = document.getElementById(`course-on-sale==${uid}==course-link`);
            console.log(courseLinkElement);
            courseLinkElement.href = `${window.location.origin}/course/${template_dependencies.course.pretty_name}-${template_dependencies.course.institution_bundle_id}`
            courseLinkElement.innerHTML = "Enroll now";
            this.saleTimer(Number(paramObj.timer), uid);
        } catch (error) {
            console.log(error);
        }
    }

    pricingOptions = async (uid) => {
        try {
            let container = document.getElementById(`pricing-options-rendering==${uid}`);
            this.addLoadingState(container);
            let template_dependencies = { feeTemplates: [], freeEnrollAllowed: false, cost: "", striked_cost: "" };
            let form = document.getElementById(`pricing-options==${uid}`);
            let paramObj = {
                bundle_id: Object.values(form)[0].value,
                institution_bundle_id: Object.values(form)[1].value,

            }
            let serviceUrl = this.apiPath + endpointMapper.getPricingOptions(paramObj);
            let response = await this.fetcher(serviceUrl);
            response = response.data;
            template_dependencies.freeEnrollAllowed = response.organization_details.free_preview_allowed;
            template_dependencies.feeTemplates = response.organization_details.bundle_template_data;
            template_dependencies.cost = response.organization_details.bundle_cost[Object.keys(response.organization_details.bundle_cost)[0]].cost;
            template_dependencies.striked_cost = response.organization_details.bundle_cost[Object.keys(response.organization_details.bundle_cost)[0]].striked_cost;
            console.log(template_dependencies);
            let renderedTemplate = await this.renderTemplate('pricing-options', template_dependencies);
            container.innerHTML = renderedTemplate;
        } catch (error) {
            console.log(error);
        }
    }


    referAndEarn = async (uid) => {
        try {
            let container = document.getElementById(`refer-n-earn-rendering==${uid}`);
            this.addLoadingState(container);
            let template_dependencies = { refObj: null, ref_code: "" };
            let form = document.getElementById(`refer-n-earn==${uid}`);
            let headers = {
                APIKEY: localStorage.getItem('apikey'),
                ORGID: Object.values(form)[0].value,
            }
            let serviceUrl = this.apiPath + endpointMapper.getCreditVariables();
            let response = await this.fetcher(serviceUrl, headers = headers);
            response = response.data;
            template_dependencies.refObj = response.variables;
            if (!response.enable_credit_system) {
                serviceUrl = this.apiPath + endpointMapper.getUserReferral();
                response = await this.fetcher(serviceUrl, headers = headers);
                response = response.data;
                template_dependencies.ref_code = response.referral_code;
            }
            let renderedTemplate = await this.renderTemplate('refer-n-earn', template_dependencies);
            container.innerHTML = renderedTemplate;
        } catch (error) {
            console.log(error);
        }
    }

    saleTimer = (inputTimeStamp, uid) => {
        let timer = setInterval(() => {
            var currTimeStamp = new Date().getTime();
            var remain = inputTimeStamp - currTimeStamp;
            console.log(remain);
            if (remain < 0) {
                console.log('inside');
                if (document.getElementById('day')) {
                    document.getElementById('day').innerHTML = 0;
                    document.getElementById('hour').innerHTML = 0;
                    document.getElementById('minute').innerHTML = 0;
                    document.getElementById('second').innerHTML = 0;
                }
                clearInterval(timer);
            }

            if (remain > 0) {
                var days = Math.floor(remain / (1000 * 60 * 60 * 24));
                var hours = Math.floor((remain % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((remain % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((remain % (1000 * 60)) / 1000);

                document.getElementById('day').innerHTML = days;
                document.getElementById('hour').innerHTML = hours;
                document.getElementById('minute').innerHTML = minutes;
                document.getElementById('second').innerHTML = seconds;
            }
        }, 1000);
        this.intervals.push(timer);
    }

    getCategoryWiseCourses = async (institution_id, categories) => {
        let asyncMap = categories.map(async (category) => {
            let catId = category.stream_id;
            let paramObj = { institution_id: institution_id, category_id: catId }
            let serviceUrl = this.apiPath + endpointMapper.getCategoryCourses(paramObj);
            let response = await this.fetcher(serviceUrl);
            response = response.data;
            let categoryCourseList = this.formCourseList(response.institute_courses, 8);
            category.courses = categoryCourseList;
        });
        const completed = await Promise.all(asyncMap);
        return completed;
    }

    allCategoriesCoursesListing = async (uid) => {
        let template_dependencies = { categories: [] }
        let container = document.getElementById(`all-categories-course-listing-rendering==${uid}`);
        this.addLoadingState(container);
        let form = document.getElementById(`all-categories-course-listing==${uid}`);
        let paramObj = {
            institution_id: Object.values(form)[0].value
        }
        let serviceUrl = this.apiPath + endpointMapper.getAllCategories(paramObj);
        let response = await this.fetcher(serviceUrl);
        response = response.data;
        template_dependencies.categories = response.category;
        if (await this.getCategoryWiseCourses(paramObj.institution_id, template_dependencies.categories))
            console.log(template_dependencies);
        let renderedTemplate = await this.renderTemplate('all-category-courses-listing', template_dependencies);
        console.log(renderedTemplate);
        container.innerHTML = renderedTemplate;
    }

    //     var endPoint = '/nuSource/api/v1/';
    //   var apiEndpoint = location.protocol + '//' + location.hostname + endPoint;
    //   let formHandle = () => {
    //     var data = {
    //         first_name: document.getElementById('name').value,
    //         email: document.getElementById('email').value,
    //         contactNumber: document.getElementById('phone').value,
    //         message: document.getElementById('message').value,
    //       };
    //       var formData = new FormData();
    //       formData.append("JSONString", JSON.stringify(data));
    //       try{
    //         const response = await fetch(apiEndpoint + 'contactus', {
    //         method: 'POST',
    //         body: formData,
    //       })
    //       if(reponse.status == 2000){
    //         showToast("Message sent successfully", 1);
    //       }
    //     }catch(error){
    //         var resp = JSON.parse(error.responseText);
    //         showToast(resp.message, 2);
    //       };
    //   }

    leadContactFormHandler(identifier) {
        let form = document.getElementById(identifier);
        form.addEventListener('submit', (event) => {
            event.preventDefault();
            this.handleFormSubmit(event);
        });
    }

    handleFormSubmit = async (e) => {
        let formData = {
            first_name: "",
            last_name: "",
            email: "",
            message: "",
            contact_number: "",
    
        };
        [...e.target].forEach((item) => {
            console.log(item.getAttribute('name'), item.value);
            let fieldName = item.getAttribute('name');
            if (fieldName) {
                formData[fieldName] = item.value;
            }
        })
        console.log(formData);
        let payload = new FormData();
        payload.append("JSONString", JSON.stringify(formData));
    
        try {
            let serviceUrl = this.apiPath + 'contactus';
            let response = await fetch(serviceUrl, {
                method: 'POST',
                body: payload,
            });
            response = await response.json()
            console.log(response);
            if (response.code == 200) {
                showToast("Message sent successfully", 1);
            } else {
                showToast(response.message, 1);  
            } 
        } catch (error) {
            console.log(error);
            showToast("An error Occured while sending your message!", 2);
        };
    }
}

showToast = (m, x=0) => {
    alert(m);
}

window.renderEngineClient = new RenderEngineClient();


window.copyToClipboard = (text) => {
    try {
        const elem = document.createElement('textarea');
        elem.value = text.trim();
        document.body.appendChild(elem);
        elem.select();
        document.execCommand('copy');
        document.body.removeChild(elem);
        showToast('Referral Code Copied!', 1);
    } catch (error) {
        showToast('Unable to Copy Code', 2)
    }
}

window.shareLink = (type, ref_code) => {
    let link = null;
    let external = false;
    let referralLink = `${INST_SETTINGS.portal_link}?referral=${ref_code}`;

    switch (type) {
        case 'facebook-share':
            link = `https://www.facebook.com/sharer/sharer.php?u=${referralLink}`;
            external = true;
            break;
        case 'whatsapp-share':
            link = `https://api.whatsapp.com/send/?phone&text=${encodeURIComponent(referralLink)}&app_absent=0`
            external = true;
            break;
        case 'manual-share':
            link = referralLink
            break;
        default:
            link = this.get('ref_code');
    }
    if (external && link) {
        window.open(link);
    } else if (link) {
        copyToClipboard(link)
    }
}

CURR_MODAL = null;

var pricingTemplate = `
<div id="InstallmentModal" class="modal fade" id="pricing-modal-1" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <div class="d-flex justify-content-between align-items-center">
                    <h2 class="ui-main-bold mb-1">Installments</h2>
                    <h2 class="ui-main-bold mb-1">Amount</h2>
                </div>
                <hr class="dropdown-divider p-0">

                <% pricing.forEach((template) => { %>
                <div>
                    <h3 class="ui-main mb-0"><%= template.name %>
                    </h3>
                    <div class="d-flex justify-content-between align-items-center">
                        <h4 class="ui-small"><%= template.desc %></h4>
                        <p class="ui-small">₹ <%= template.amount %></p>
                    </div>
                </div>
                <% }) %>
            </div>
        </div>
    </div>
</div>
`;

let shutModalOnOutClick = () => {
    document.getElementById('InstallmentModal').addEventListener('hidden.bs.modal', function (event) {
        document.getElementById('InstallmentModal').remove();
    });
}

window.renderInstallmentDetails = (element) => {
    let template = pricingTemplate;
    let template_data = JSON.parse(decodeURIComponent(element.getAttribute('templates-data')));
    console.log(typeof template_data);
    console.log(template_data);
    let temp = [];
    for (let i = 0; i < template_data.length; i++) {
        console.log("TEMPLATE");
        console.log(template_data[i]);
        obj = {};
        if (template_data[i][0] != 0) {
            obj.desc = "After " + template_data[i][0] + " day";
        }
        else if (template_data[i][1] != 0) {
            obj.desc = "After " + template_data[i][1] + " month";
        } else {
            obj.desc = "";
        }
        obj.amount = template_data[i][2];
        obj.name = template_data[i][3];
        temp.push(obj);
    }
    console.log(temp);

    templateDependencies = {
        pricing: temp,
    }
    document.body.innerHTML += ejs.render(template, templateDependencies);
    if (CURR_MODAL) CURR_MODAL.hide();
    CURR_MODAL = new bootstrap.Modal(document.getElementById('InstallmentModal'), { backdrop: true, keyboard: true, focus: true })
    CURR_MODAL.show();
    shutModalOnOutClick();
}